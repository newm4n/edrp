package com.difotofoto.myeddb.io;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public class EnhancedDataOutputStream extends DataOutputStream {
    public EnhancedDataOutputStream(OutputStream out) {
        super(out);
    }

    public void writeString(String string) throws IOException {
        if (string == null) {
            this.writeByte(-1);
        } else {
            byte[] b = string.getBytes();
            this.writeByte(b.length);
            this.write(b);
        }
    }

    public void writeIntegerObject(Integer integer) throws IOException {
        if (integer == null) {
            this.writeInt(Integer.MAX_VALUE);
        } else {
            this.writeInt(integer);
        }
    }

    public void writeLongObject(Long l) throws IOException {
        if (l == null) {
            this.writeLong(Long.MAX_VALUE);
        } else {
            this.writeLong(l);
        }
    }

    public void writeShortObject(Short s) throws IOException {
        if (s == null) {
            this.writeShort(Short.MAX_VALUE);
        } else {
            this.writeShort(s);
        }
    }

    public void writeByteObject(Byte b) throws IOException {
        if (b == null) {
            this.writeByte(Byte.MAX_VALUE);
        } else {
            this.writeByte(b);
        }
    }

    public void writeFloatObject(Float f) throws IOException {
        if (f == null) {
            this.writeFloat(Float.MAX_VALUE);
        } else {
            this.writeFloat(f);
        }
    }

    public void writeDoubleObject(Double d) throws IOException {
        if (d == null) {
            this.writeDouble(Double.MAX_VALUE);
        } else {
            this.writeDouble(d);
        }
    }

    public void writeBooleanObject(Boolean b) throws IOException {
        if (b == null) {
            writeByte(0);
        } else {
            if (b) {
                writeByte(1);
            } else {
                writeByte(2);
            }
        }
    }
}
