package com.difotofoto.myeddb.io;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public class EnhancedDataInputStream extends DataInputStream {
    public EnhancedDataInputStream(InputStream in) {
        super(in);
    }

    public String readString() throws IOException {
        byte len = this.readByte();
        if (len == -1) return null;
        byte[] b = new byte[len];
        this.readFully(b);
        return new String(b);
    }

    public Integer readIntegerObject() throws IOException {
        int i = super.readInt();
        if (i == Integer.MAX_VALUE) {
            return null;
        }
        return i;
    }

    public Short readShortObject() throws IOException {
        short s = super.readShort();
        if (s == Short.MAX_VALUE) {
            return null;
        }
        return s;
    }

    public Long readLongObject() throws IOException {
        long l = super.readLong();
        if (l == Long.MAX_VALUE) {
            return null;
        }
        return l;
    }

    public Byte readByteObject() throws IOException {
        byte b = super.readByte();
        if (b == Byte.MAX_VALUE) {
            return null;
        }
        return b;
    }

    public Float readFloatObject() throws IOException {
        float f = super.readFloat();
        if (f == Float.MAX_VALUE) {
            return null;
        }
        return f;
    }

    public Double readDoubleObject() throws IOException {
        double d = super.readDouble();
        if (d == Double.MAX_VALUE) {
            return null;
        }
        return d;
    }

    public Boolean readBooleanObject() throws IOException {
        byte b = super.readByte();
        if (b == 0) {
            return null;
        } else if (b == 1) {
            return true;
        } else {
            return false;
        }
    }

}
