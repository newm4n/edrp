package com.difotofoto.myeddb;

import com.difotofoto.myeddb.importer.*;
import com.difotofoto.myeddb.io.EnhancedDataInputStream;
import com.difotofoto.myeddb.io.EnhancedDataOutputStream;
import com.difotofoto.myeddb.model.*;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ferdinand on 3/10/2016.
 */
public class Graph implements Streamable {

    public static Logger log = Logger.getLogger(Graph.class);

    public Date graphDate;
    public Map<Integer, StarSystem> systems = new HashMap<Integer, StarSystem>();
    public Map<Integer, Station> stations = new HashMap<Integer, Station>();
    public Map<Integer, Commodity> commodities = new HashMap<Integer, Commodity>();
    public Map<Integer, Market> markets = new HashMap<Integer, Market>();

    public void httpsLoad() {
        log.info("Start Loading");

        getSystems().clear();
        getStations().clear();
        getCommodities().clear();
        getMarkets().clear();

        long start = System.currentTimeMillis();
        CommodityImporter imp = new CommodityImporter();
        MarketImporter mimp = new MarketImporter();
        StarSystemImporter starImp = new StarSystemImporter();
        StationImporter stationImp = new StationImporter();
        try {
            systems = starImp.importJson();
            stations = stationImp.importJson();

            for (Station station : stations.values()) {
                StarSystem ss = systems.get(station.getSystemId());
                if (ss != null) {
                    station.setStarSystem(ss);
                    ss.getStations().add(station);
                } else {
                    log.warn(station.getName() + " has unknown star system");
                }
            }

            commodities = imp.importJson();
            markets = mimp.importMarket(commodities, stations);
        } catch (IOException e) {
            e.printStackTrace();
        }
        log.info("Finish import in " + (System.currentTimeMillis() - start) + " ms");
    }

    public void load(InputStream inputStream) throws IOException {
        EnhancedDataInputStream edis = new EnhancedDataInputStream(inputStream);
        readFromStream(edis);
    }

    public void save(OutputStream outputStream) throws IOException {
        EnhancedDataOutputStream edos = new EnhancedDataOutputStream(outputStream);
        writeToStream(edos);
    }

    public void save(File file) throws IOException {
        if (file.exists()) {
            file.delete();
        }
        file.createNewFile();
        FileOutputStream fos = new FileOutputStream(file);
        save(fos);
        fos.flush();
        fos.close();
    }

    public void load(File file) throws IOException {
        if (file.exists() == false || file.isFile() == false) {
            throw new FileNotFoundException();
        }
        FileInputStream fis = new FileInputStream(file);
        load(fis);
        fis.close();
    }

    private static StarSystem findStarSystem(List<StarSystem> systems, int id) {
        for (StarSystem com : systems) {
            if (com.getId() == id) return com;
        }
        return null;
    }

    public Map<Integer, StarSystem> getSystems() {
        return systems;
    }

    public Map<Integer, Station> getStations() {
        return stations;
    }

    public Map<Integer, Commodity> getCommodities() {
        return commodities;
    }

    public Map<Integer, Market> getMarkets() {
        return markets;
    }

    public void setSystems(Map<Integer, StarSystem> systems) {
        this.systems = systems;
    }

    public void setStations(Map<Integer, Station> stations) {
        this.stations = stations;
    }

    public void setCommodities(Map<Integer, Commodity> commodities) {
        this.commodities = commodities;
    }

    public void setMarkets(Map<Integer, Market> markets) {
        this.markets = markets;
    }

    public static String GRAPH_VERSION = "MYEDDIB-GRAPH-1.2";

    DownloadProgressListener progressListener = null;

    public DownloadProgressListener getProgressListener() {
        return progressListener;
    }

    public void setProgressListener(DownloadProgressListener progressListener) {
        this.progressListener = progressListener;
    }

    @Override
    public void readFromStream(EnhancedDataInputStream inputStream) throws IOException {
        String version = inputStream.readString();
        if (!version.equals(GRAPH_VERSION)) {
            throw new IOException("Invalid graph version.");
        }
        setGraphDate(new Date(inputStream.readLong()));
        getSystems().clear();
        getStations().clear();
        getCommodities().clear();
        getMarkets().clear();

        int commodityCount = inputStream.readInt();
        for (int i = 0; i < commodityCount; i++) {
            Commodity commodity = new Commodity();
            commodity.readFromStream(inputStream);
            commodities.put(commodity.getId(), commodity);
        }

        int syscount = inputStream.readIntegerObject();
        if (progressListener != null) {
            progressListener.contentLength(syscount);
            progressListener.start();
        }
        for (int i = 0; i < syscount; i++) {
            if (progressListener != null) progressListener.downloaded(i + 1);
            StarSystem system = new StarSystem();
            system.setGraph(this);
            system.readFromStream(inputStream);
            systems.put(system.getId(), system);
        }
        if (progressListener != null) progressListener.finish();
    }

    @Override
    public void writeToStream(EnhancedDataOutputStream outputStream) throws IOException {
        int counter = 0;

        int systemCount = 0;
        for (StarSystem system : systems.values()) {
            if (system.getHasStation() && system.getHasStationWithMarket())
                systemCount++;
        }

        int totalWork = systemCount + commodities.size();
        if (progressListener != null) {
            progressListener.contentLength(totalWork + 1);
            progressListener.start();
        }


        outputStream.writeString(GRAPH_VERSION);
        outputStream.writeLong(getGraphDate().getTime());
        outputStream.writeInt(commodities.size());
        for (Commodity commodity : commodities.values()) {
            counter++;
            if (progressListener != null) progressListener.downloaded(counter);
            commodity.writeToStream(outputStream);
        }


        outputStream.writeIntegerObject(systemCount);
        for (StarSystem system : systems.values()) {
            if (system.getHasStation() && system.getHasStationWithMarket()) {
                counter++;
                if (progressListener != null) progressListener.downloaded(counter);
                system.writeToStream(outputStream);
            }
        }
    }

    public Date getGraphDate() {
        return graphDate;
    }

    public void setGraphDate(Date graphDate) {
        this.graphDate = graphDate;
    }
}
