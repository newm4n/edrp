package com.difotofoto.myeddb.analyzer;

import com.difotofoto.myeddb.model.*;

/**
 * Created by ferdinand on 3/15/2016.
 */
public class Criteria {
    private Long minimumProfit;
    private Double maximumDistance;
    private PowerEnum[] powers;
    private GovernmentEnum[] governments;
    private AllegianceEnum[] allegiances;
    private boolean needPermit;

    private boolean planetary;
    private LandingPadEnum landingPad; // S, M, L
    private Integer minSupply;
    private Integer minDemand;
    private Integer maximumStarDistance;

    public boolean isPass(Station a, Station b, TradeLoop abLoop) {
//        return isDistancePass(a, b) && isStarDistancePass(a, b) &&
//                isPowerPass(a, b) && isGovernmentPass(a, b) &&
//                isAllegiancePass(a, b) && isPermitPass(a, b) &&
//                isPlanetaryPass(a, b) && isLandingPadPass(a, b) && isProfitPass(a, b, abLoop);
        return isDistancePass(a, b) && isStarDistancePass(a, b) && isProfitPass(a, b, abLoop) && isLandingPadPass(a, b);
    }

    public boolean isProfitPass(Station a, Station b, TradeLoop abLoop) {
        if (minimumProfit == null) return true;
        return abLoop.getTotalProfit() > minimumProfit;
    }

    public boolean isDistancePass(Station a, Station b) {
        if (maximumDistance == null) return true;
        return a.getStarSystem().distanceTo(b.getStarSystem()) < maximumDistance;
    }

    public boolean isPowerPass(Station a, Station b) {
        if (powers == null || powers.length == 0) return true;
        for (PowerEnum p : powers) {
            if (p.equals(a.getStarSystem().getPowerEnum()) && p.equals(b.getStarSystem().getPowerEnum())) {
                return true;
            }
        }
        return false;
    }

    public boolean isGovernmentPass(Station a, Station b) {
        if (governments == null || governments.length == 0) return true;
        for (GovernmentEnum p : governments) {
            if (p.equals(a.getStarSystem().getGovernmentEnum()) && p.equals(b.getStarSystem().getGovernmentEnum())) {
                return true;
            }
        }
        return false;
    }

    public boolean isAllegiancePass(Station a, Station b) {
        if (allegiances == null || allegiances.length == 0) return true;
        for (AllegianceEnum p : allegiances) {
            if (p.equals(a.getStarSystem().getAllegianceEnum()) && p.equals(b.getStarSystem().getAllegianceEnum())) {
                return true;
            }
        }
        return false;
    }

    public boolean isPermitPass(Station a, Station b) {
        if (needPermit) return true;
        return !(a.getStarSystem().isNeedPermit() || b.getStarSystem().isNeedPermit());
    }

    public boolean isPlanetaryPass(Station a, Station b) {
        if (planetary) return true;
        return !(a.isPlannetary() || b.isPlannetary());
    }

    public boolean isLandingPadPass(Station a, Station b) {
        if (landingPad == null) return true;
        if (a.getLandingPad() == null || b.getLandingPad() == null) return false;
        return a.getLandingPad().ordinal() <= landingPad.ordinal() && b.getLandingPad().ordinal() <= landingPad.ordinal();
    }

    public boolean isStarDistancePass(Station a, Station b) {
        if (getMaximumStarDistance() == null) return true;
        if (a.getDistanceToStar() != null && b.getDistanceToStar() != null) {
            return a.getDistanceToStar() < getMaximumStarDistance() && b.getDistanceToStar() < getMaximumStarDistance();
        } else {
            return false;
        }
    }


    private boolean isStringArrayContainString(String[] arr, String search) {
        if (arr == null && search == null) return true;
        if (arr == null || search == null) return false;
        for (String s : arr) {
            if (s.equalsIgnoreCase(search)) return true;
        }
        return false;
    }

    private Commodity[] ignoredCommodities;

    public Long getMinimumProfit() {
        return minimumProfit;
    }

    public void setMinimumProfit(Long minimumProfit) {
        this.minimumProfit = minimumProfit;
    }

    public Double getMaximumDistance() {
        return maximumDistance;
    }

    public void setMaximumDistance(Double maximumDistance) {
        this.maximumDistance = maximumDistance;
    }

    public PowerEnum[] getPowers() {
        return powers;
    }

    public void setPowers(PowerEnum[] powers) {
        this.powers = powers;
    }

    public GovernmentEnum[] getGovernments() {
        return governments;
    }

    public void setGovernments(GovernmentEnum[] governments) {
        this.governments = governments;
    }

    public AllegianceEnum[] getAllegiances() {
        return allegiances;
    }

    public void setAllegiances(AllegianceEnum[] allegiances) {
        this.allegiances = allegiances;
    }

    public boolean isNeedPermit() {
        return needPermit;
    }

    public void setNeedPermit(boolean needPermit) {
        this.needPermit = needPermit;
    }

    public boolean isPlanetary() {
        return planetary;
    }

    public void setPlanetary(boolean planetary) {
        this.planetary = planetary;
    }

    public LandingPadEnum getLandingPad() {
        return landingPad;
    }

    public void setLandingPad(LandingPadEnum landingPad) {
        this.landingPad = landingPad;
    }

    public Integer getMinSupply() {
        if (minSupply == null) return 0;
        return minSupply;
    }

    public void setMinSupply(Integer minSupply) {
        this.minSupply = minSupply;
    }

    public Integer getMinDemand() {
        if (minDemand == null) return 0;
        return minDemand;
    }

    public void setMinDemand(Integer minDemand) {
        this.minDemand = minDemand;
    }

    public Commodity[] getIgnoredCommodities() {
        return ignoredCommodities;
    }

    public void setIgnoredCommodities(Commodity[] ignoredCommodities) {
        this.ignoredCommodities = ignoredCommodities;
    }

    public Integer getMaximumStarDistance() {
        return maximumStarDistance;
    }

    public void setMaximumStarDistance(Integer maximumStarDistance) {
        this.maximumStarDistance = maximumStarDistance;
    }
}
