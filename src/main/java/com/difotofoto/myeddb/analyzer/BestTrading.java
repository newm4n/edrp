package com.difotofoto.myeddb.analyzer;

import com.difotofoto.myeddb.Graph;
import com.difotofoto.myeddb.importer.ProgressBarProgressListener;
import com.difotofoto.myeddb.model.*;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ferdinand on 3/10/2016.
 */
public class BestTrading {

    public static Logger log = Logger.getLogger(BestTrading.class);

    private static double calcPairHash(int a, int b) {
        double ad = a;
        double bd = b;
        return ((ad + bd) / ad) + ((ad + bd) / bd);
    }

    public static List<TradeLoop> bestShortRangeTradingV1(Graph graph) {

        ProgressBarProgressListener pbpl = new ProgressBarProgressListener("Analyzing");
        pbpl.contentLength(graph.getStations().size());
        pbpl.start();

        List<TradeLoop> tradeRoutes = new ArrayList<TradeLoop>();
        int count = 0;
        long percent = 0;
        for (Station sa : graph.getStations().values()) {
            count++;
            pbpl.downloaded(count);
            for (Station sb : graph.getStations().values()) {
                if (sa.getStarSystem().distanceTo(sb.getStarSystem()) < 11d &&
                        sa.getLandingPad() != null && sb.getLandingPad() != null && sa.getDistanceToStar() != null &&
                        sb.getDistanceToStar() != null && sa.isPlannetary() != null && sb.isPlannetary() == false &&
                        sa.getLandingPad() == LandingPadEnum.L &&
                        sb.getLandingPad() == LandingPadEnum.L &&
                        sb.getDistanceToStar() < 500 &&
                        sa.getDistanceToStar() < 500 &&
                        sa.isPlannetary() == false &&
                        sb.isPlannetary() == false) {
                    TradeLoop loop = bestTradeBetweenTwoStation(sa, sb, 15000, 0);
                    if (loop != null && loop.getTotalProfit() < 3800) {
                        tradeRoutes.add(loop);
                    }
                }
            }
            long newpercent = Math.round(((double) count) / ((double) graph.getStations().size()) * 100d);
            if (newpercent != percent) {
                percent = newpercent;
                log.debug(percent + "%");
            }
        }
        Collections.sort(tradeRoutes);
        List<TradeLoop> finalRoutes = new ArrayList<TradeLoop>();
        for (int x = 0; x < (tradeRoutes.size() > 30 ? 30 : tradeRoutes.size()); x += 2) {
            finalRoutes.add(tradeRoutes.get(x));
        }

        pbpl.finish();
        return finalRoutes;
    }

    public static List<TradeLoop> bestShortRangeTrading(Graph graph, Criteria criteria) {
        List<TradeLoop> tradeRoutes = new ArrayList<TradeLoop>();
        List<Double> check = new ArrayList<Double>();
        int count = 0;
        long percent = 0;

        long cc = 0;
        long tot = 0;

        for (Station sa : graph.getStations().values()) {
            if (!sa.isHasMarket()) continue;
            count++;
            for (Station sb : graph.getStations().values()) {
                if (!sb.isHasMarket()) continue;

//                Double ip = calcPairHash(sa.getId(), sb.getId());
//                if (check.contains(ip)) continue;
//                check.add(ip);

                TradeLoop loop = bestTradeBetweenTwoStation(sa, sb, criteria.getMinSupply(), criteria.getMinDemand());
                if (criteria.isPass(sa, sb, loop)) {
                    tradeRoutes.add(loop);
                    //log.debug(loop.toString());
                }

//                cc++;
//                if(cc % 2000 == 0) {
//                    log.debug("Tick " + System.currentTimeMillis());
//                }
            }
            long newpercent = Math.round(((double) count) / ((double) graph.getStations().size()) * 100d);
            if (newpercent != percent) {
                percent = newpercent;
                log.debug(percent + "%");
            }
        }
        Collections.sort(tradeRoutes);

        return tradeRoutes;
    }

    public static TradeLoop bestTradeBetweenTwoStation(Station a, Station b, int minSupply, int minDemand) {
        Trade abMost = null;
        Trade baMost = null;
        long abMax = 0;
        long baMax = 0;

        for (Market ma : a.getMarkets()) {
            Market mb = b.getMarketForCommodity(ma.getCommodity());
            if (mb != null) {
                if (ma.getBuy() > 0 && ma.getSupply() >= minSupply && mb.getDemand() >= minDemand) {
                    Trade ab = new Trade();
                    ab.setBuyPrice(ma.getBuy());
                    ab.setSellPrice(mb.getSell());
                    ab.setBuyStation(a);
                    ab.setSellStation(b);
                    ab.setCommodity(ma.getCommodity());
                    if (abMost == null) {
                        abMost = ab;
                        abMax = ab.getTradeProfit();
                    } else {
                        long abm = ab.getTradeProfit();
                        if (abm > abMax) {
                            abMost = ab;
                            abMax = abm;
                        }
                    }
                }
                if (mb.getBuy() > 0 && mb.getSupply() >= minSupply && ma.getDemand() >= minDemand) {
                    Trade ba = new Trade();
                    ba.setBuyPrice(mb.getBuy());
                    ba.setSellPrice(ma.getSell());
                    ba.setBuyStation(b);
                    ba.setSellStation(a);
                    ba.setCommodity(ma.getCommodity());
                    if (baMost == null) {
                        baMost = ba;
                        baMax = ba.getTradeProfit();
                    } else {
                        long bam = ba.getTradeProfit();
                        if (bam > baMax) {
                            baMost = ba;
                            baMax = bam;
                        }
                    }
                }
            }
        }
        if (abMost != null && baMost != null) {
            TradeLoop loop = new TradeLoop();
            loop.add(abMost);
            loop.add(baMost);
            return loop;
        } else {
            return null;
        }
    }

}
