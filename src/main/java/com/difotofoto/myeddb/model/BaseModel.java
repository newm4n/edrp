package com.difotofoto.myeddb.model;

import com.difotofoto.myeddb.Graph;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class BaseModel {
    private int id;
    private Graph graph;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setGraph(Graph graph) {
        this.graph = graph;
    }

    protected Graph getGraph() {
        return graph;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BaseModel baseModel = (BaseModel) o;

        return id == baseModel.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
