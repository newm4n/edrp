package com.difotofoto.myeddb.model;

import com.difotofoto.myeddb.importer.serializer.NumericBooleanDeserializer;
import com.difotofoto.myeddb.importer.serializer.NumericBooleanSerializer;
import com.difotofoto.myeddb.io.EnhancedDataInputStream;
import com.difotofoto.myeddb.io.EnhancedDataOutputStream;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ferdinand on 3/10/2016.
 */

/*
{
    "id":9285,
    "name":"Laing Dock",
    "system_id":1041,
    "max_landing_pad_size":"M",
    "distance_to_star":18224,
    "faction":"Social Antilope Future",
    "government":"Democracy",
    "allegiance":"Independent",
    "state":"None",
    "type_id":4,
    "type":"Industrial Outpost",
    "has_blackmarket":1,
    "has_market":1,
    "has_refuel":1,
    "has_repair":1,
    "has_rearm":0,
    "has_outfitting":0,
    "has_shipyard":0,
    "has_commodities":1,
    "import_commodities":["Aluminium","Leather","Synthetic Fabrics"],
    "export_commodities":["Hydrogen Fuel","Food Cartridges","Power Generators"],
    "prohibited_commodities":["Narcotics","Combat Stabilisers","Imperial Slaves","Slaves","Personal Weapons","Battle Weapons","Toxic Waste","Bootleg Liquor","Landmines"],
    "economies":["Industrial"],
    "updated_at":1453133164,
    "shipyard_updated_at":null,
    "outfitting_updated_at":null,
    "market_updated_at":1455369390,
    "is_planetary":0,
    "selling_ships":[],
    "selling_modules":[]
 },{
    "id":9286,
    "name":"Irens Terminal",
    "system_id":1041,
    "max_landing_pad_size":"L",
    "distance_to_star":17586,
    "faction":"Antilope Blue Energy Corporation",
    "government":"Dictatorship",
    "allegiance":"Independent",
    "state":null,
    "type_id":12,
    "type":"Unknown Starport",
    "has_blackmarket":0,
    "has_market":1,
    "has_refuel":1,
    "has_repair":1,
    "has_rearm":1,
    "has_outfitting":1,
    "has_shipyard":1,
    "has_commodities":1,
    "import_commodities":[],
    "export_commodities":[],
    "prohibited_commodities":[],
    "economies":["Industrial"],
    "updated_at":1444493809,
    "shipyard_updated_at":1455283052,
    "outfitting_updated_at":1455283046,
    "market_updated_at":1455283044,
    "is_planetary":0,
    "selling_ships":["Asp Explorer","Eagle Mk. II","Fer-de-Lance","Hauler","Sidewinder Mk. I","Type-6 Transporter","Type-7 Transporter","Type-9 Heavy","Vulture","Asp Scout","Keelback"],
    "selling_modules":[738,739,740,741,742,743,744,745,748,749,750,768,769,770,771,772,773,774,775,776,777,778,779,780,781,782,783,784,785,786,787,803,804,805,806,807,813,814,815,816,817,824,825,826,827,828,830,834,835,838,839,840,841,842,845,846,847,851,852,854,855,857,858,859,860,862,863,864,865,866,867,868,869,870,871,872,873,874,875,876,877,878,879,880,882,883,884,886,887,888,889,890,891,892,893,894,895,896,897,898,900,903,904,905,908,909,910,912,913,920,923,926,927,928,929,930,931,932,933,934,936,937,938,939,940,941,942,943,945,946,947,948,950,951,952,954,955,956,957,958,959,961,962,963,965,966,967,968,969,970,974,975,979,980,983,984,985,988,989,994,997,998,999,1000,1002,1003,1004,1005,1006,1007,1008,1009,1011,1012,1013,1014,1015,1016,1017,1018,1019,1020,1021,1022,1024,1026,1027,1028,1029,1030,1031,1032,1034,1035,1036,1037,1038,1039,1041,1042,1043,1044,1045,1046,1047,1048,1050,1053,1054,1055,1057,1058,1059,1061,1062,1065,1066,1067,1068,1069,1071,1072,1073,1074,1076,1077,1078,1079,1080,1081,1082,1083,1085,1086,1087,1088,1089,1090,1091,1092,1093,1094,1095,1096,1097,1098,1099,1100,1101,1102,1103,1104,1105,1107,1108,1110,1111,1112,1113,1116,1117,1118,1119,1120,1121,1122,1123,1124,1125,1126,1127,1128,1129,1130,1131,1132,1134,1136,1137,1138,1139,1141,1142,1143,1146,1148,1149,1154,1155,1159,1163,1164,1165,1168,1170,1172,1173,1174,1175,1176,1177,1179,1181,1182,1185,1186,1187,1189,1191,1192,1193,1194,1195,1196,1197,1198,1199,1200,1201,1202,1203,1204,1205,1206,1207,1208,1209,1211,1212,1213,1214,1219,1222,1223,1227,1228,1229,1230,1232,1233,1235,1237,1238,1239,1241,1242,1243,1244,1245,1246,1247,1248,1249,1250,1251,1254,1255,1256,1257,1258,1259,1260,1261,1262,1263,1264,1265,1266,1267,1269,1270,1271,1273,1274,1275,1278,1279,1281,1282,1284,1286,1287,1290,1291,1294,1295,1297,1298,1299,1300,1301,1303,1304,1305,1306,1307,1308,1309,1310,1311,1312,1313,1316,1317,1318,1319,1320,1321,1324,1325,1326,1327,1331,1332,1333,1334,1335,1337,1338,1339,1340,1341,1344,1345,1346,1350,1351,1353,1358,1359,1363,1368,1369,1370,1371,1372,1373,1374,1375,1376,1378,1379,1380,1381,1382,1383,1394,1395,1396,1397,1398,1399,1400,1401,1402,1403,1404,1405,1406,1407,1408,1409,1410,1411,1412,1413,1414,1415,1416,1417,1418,1419,1420,1421,1423,1424,1425,1426,1428,1429,1430,1432,1433,1434,1435,1436,1437,1438,1439,1440,1441,1442,1443,1444,1445,1446,1447,1448,1449,1450,1451,1453,1455,1503,1504,1505,1506,1507,1513,1514,1515,1516,1517,1523,1530,1531,1532,1533,1534,1535,1536,1537]}
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class Station extends BaseModel implements Streamable, Comparable<Station> {
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "max_landing_pad_size")
    private String maxLandingPadSize;

    @JsonIgnoreProperties
    private LandingPadEnum landingPad;

    @JsonProperty(value = "distance_to_star")
    private Integer distanceToStar;
    private StarSystem starSystem;

    @JsonProperty(value = "system_id")
    private Integer systemId;
    private List<Market> markets = new ArrayList<Market>();

    @JsonProperty(value = "faction")
    private String faction;

    @JsonProperty(value = "government")
    private String government;

    @JsonProperty(value = "allegiance")
    private String allegiance;

    @JsonProperty(value = "type")
    private String type;

    @JsonProperty(value = "is_planetary")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean plannetary;


    @JsonProperty(value = "has_blackmarket")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasBlackMarket;

    @JsonProperty(value = "has_market")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasMarket;

    @JsonProperty(value = "has_refuel")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasRefuel;

    @JsonProperty(value = "has_repair")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasRepair;

    @JsonProperty(value = "has_rearm")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasRearm;

    @JsonProperty(value = "has_outfitting")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasOutfitting;

    @JsonProperty(value = "has_shipyard")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasShipyard;

    @JsonProperty(value = "has_commodities")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean hasCommodities;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMaxLandingPadSize() {
        return maxLandingPadSize;
    }

    public void setMaxLandingPadSize(String maxLandingPadSize) {
        this.maxLandingPadSize = maxLandingPadSize;
        if (maxLandingPadSize != null)
            this.landingPad = LandingPadEnum.valueOf(maxLandingPadSize);
    }

    public Integer getDistanceToStar() {
        return distanceToStar;
    }

    public void setDistanceToStar(Integer distanceToStar) {
        this.distanceToStar = distanceToStar;
    }

    public List<Market> getMarkets() {
        return markets;
    }

    public void setMarkets(List<Market> markets) {
        this.markets = markets;
    }

    public StarSystem getStarSystem() {
        return starSystem;
    }

    public void setStarSystem(StarSystem starSystem) {
        this.starSystem = starSystem;
    }

    public Integer getSystemId() {
        return systemId;
    }

    public void setSystemId(Integer systemId) {
        this.systemId = systemId;
    }

    public boolean isTradingCommodity(Commodity commodity) {
        for (Market m : getMarkets()) {
            if (m.getCommodity().equals(commodity)) {
                return true;
            }
        }
        return false;
    }

    public Market getMarketForCommodity(Commodity commodity) {
        for (Market m : getMarkets()) {
            if (m.getCommodity().equals(commodity)) {
                return m;
            }
        }
        return null;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
    }

    public String getAllegiance() {
        return allegiance;
    }

    public void setAllegiance(String allegiance) {
        this.allegiance = allegiance;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean isPlannetary() {
        if (plannetary == null) return true;
        return plannetary;
    }

    public void setPlannetary(Boolean plannetary) {
        this.plannetary = plannetary;
    }

    public Boolean isHasBlackMarket() {
        if (hasBlackMarket == null) return true;
        return hasBlackMarket;
    }

    public void setHasBlackMarket(Boolean hasBlackMarket) {
        this.hasBlackMarket = hasBlackMarket;
    }

    public Boolean isHasMarket() {
        if (hasMarket == null) return true;
        return hasMarket;
    }

    public void setHasMarket(Boolean hasMarket) {
        this.hasMarket = hasMarket;
    }

    public Boolean isHasRefuel() {
        if (hasRefuel == null) return true;
        return hasRefuel;
    }

    public void setHasRefuel(Boolean hasRefuel) {
        this.hasRefuel = hasRefuel;
    }

    public Boolean isHasRepair() {
        if (hasRepair == null) return true;
        return hasRepair;
    }

    public void setHasRepair(Boolean hasRepair) {
        this.hasRepair = hasRepair;
    }

    public Boolean isHasRearm() {
        if (hasRearm == null) return true;
        return hasRearm;
    }

    public void setHasRearm(Boolean hasRearm) {
        this.hasRearm = hasRearm;
    }

    public Boolean isHasOutfitting() {
        if (hasOutfitting == null) return true;
        return hasOutfitting;
    }

    public void setHasOutfitting(Boolean hasOutfitting) {
        this.hasOutfitting = hasOutfitting;
    }

    public Boolean isHasShipyard() {
        if (hasShipyard == null) return true;
        return hasShipyard;
    }

    public void setHasShipyard(Boolean hasShipyard) {
        this.hasShipyard = hasShipyard;
    }

    public Boolean isHasCommodities() {
        if (hasCommodities == null) return true;
        return hasCommodities;
    }

    public void setHasCommodities(Boolean hasCommodities) {
        this.hasCommodities = hasCommodities;
    }

    public LandingPadEnum getLandingPad() {
        return landingPad;
    }

    public void setLandingPad(LandingPadEnum landingPad) {
        this.landingPad = landingPad;
    }

    @Override
    public void readFromStream(EnhancedDataInputStream inputStream) throws IOException {
        setId(inputStream.readIntegerObject());
        setName(inputStream.readString());
        setMaxLandingPadSize(inputStream.readString());
        setDistanceToStar(inputStream.readIntegerObject());
        setSystemId(inputStream.readIntegerObject());
        setFaction(inputStream.readString());
        setGovernment(inputStream.readString());
        setAllegiance(inputStream.readString());
        setType(inputStream.readString());
        setPlannetary(inputStream.readBooleanObject());
        setHasBlackMarket(inputStream.readBooleanObject());
        setHasMarket(inputStream.readBooleanObject());
        setHasRefuel(inputStream.readBooleanObject());
        setHasRepair(inputStream.readBooleanObject());
        setHasRearm(inputStream.readBooleanObject());
        setHasOutfitting(inputStream.readBooleanObject());
        setHasShipyard(inputStream.readBooleanObject());
        setHasBlackMarket(inputStream.readBooleanObject());

        int mCount = inputStream.readIntegerObject();
        getMarkets().clear();
        for (int i = 0; i < mCount; i++) {
            Market market = new Market();
            market.setGraph(getGraph());
            market.readFromStream(inputStream);
            getMarkets().add(market);
            market.setStation(this);
            getGraph().markets.put(market.getId(), market);
        }
    }

    @Override
    public void writeToStream(EnhancedDataOutputStream outputStream) throws IOException {
        outputStream.writeIntegerObject(getId());
        outputStream.writeString(getName());
        outputStream.writeString(getMaxLandingPadSize());
        outputStream.writeIntegerObject(getDistanceToStar() == null ? Integer.MAX_VALUE : getDistanceToStar());
        outputStream.writeIntegerObject(getSystemId() == null ? Integer.MAX_VALUE : getSystemId());
        outputStream.writeString(getFaction());
        outputStream.writeString(getGovernment());
        outputStream.writeString(getAllegiance());
        outputStream.writeString(getType());
        outputStream.writeBooleanObject(isPlannetary());
        outputStream.writeBooleanObject(isHasBlackMarket());
        outputStream.writeBooleanObject(isHasMarket());
        outputStream.writeBooleanObject(isHasRefuel());
        outputStream.writeBooleanObject(isHasRepair());
        outputStream.writeBooleanObject(isHasRearm());
        outputStream.writeBooleanObject(isHasOutfitting());
        outputStream.writeBooleanObject(isHasShipyard());
        outputStream.writeBooleanObject(isHasCommodities());

        outputStream.writeInt(getMarkets().size());
        for (Market market : getMarkets()) {
            market.writeToStream(outputStream);
        }
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(Station that) {
        return this.getName().compareTo(that.getName());
    }
}
