package com.difotofoto.myeddb.model;

/**
 * Created by Ferdinand on 3/27/2016.
 */
public enum GovernmentEnum {
    NONE("None"),
    ANARCHY("Anarchy"),
    COMMUNISM("Communism"),
    CONFEDERACY("Confederacy"),
    COOPERATIVE("Cooperative"),
    CORPORATE("Corporate"),
    DEMOCRACY("Democracy"),
    DICTATORSHIP("Dictatorship"),
    FEUDAL("Feudal"),
    PATRONAGE("Patronage"),
    PRISON_COLONY("Prison Colony"),
    THEOCRACY("Theocracy");


    private GovernmentEnum(String printName) {
        this.printName = printName;
    }

    private String printName;

    public String getPrintName() {
        return printName;
    }

    public static GovernmentEnum getByPrintName(String printName) {
        for (GovernmentEnum pe : GovernmentEnum.values()) {
            if (pe.getPrintName().equals(printName)) return pe;
        }
        return null;
    }
}
