package com.difotofoto.myeddb.model;

/**
 * Created by ferdinand on 3/10/2016.
 */
public class IdPair {

    int a;
    int b;

    public IdPair(int a, int b) {
        this.a = a;
        this.b = b;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        IdPair that = (IdPair) o;

        if (this.a == that.a && this.b == that.b) {
            return true;
        } else return this.a == that.b && this.b == that.a;
    }

    @Override
    public int hashCode() {
        int result = a + b;
        result = 31 * result;
        return result;
    }
}
