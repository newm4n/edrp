package com.difotofoto.myeddb.model;

import com.difotofoto.myeddb.importer.serializer.NumericBooleanDeserializer;
import com.difotofoto.myeddb.importer.serializer.NumericBooleanSerializer;
import com.difotofoto.myeddb.io.EnhancedDataInputStream;
import com.difotofoto.myeddb.io.EnhancedDataOutputStream;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;

/**
 * Created by Ferdinand on 3/10/2016.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Commodity extends BaseModel implements Streamable {

    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "average_price")
    private Long averagePrice;

    @JsonProperty(value = "is_rare")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean rare;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getAveragePrice() {
        return averagePrice;
    }

    public void setAveragePrice(Long averagePrice) {
        this.averagePrice = averagePrice;
    }

    public Boolean isRare() {
        return rare;
    }

    public void setRare(Boolean rare) {
        this.rare = rare;
    }

    @Override
    public String toString() {
        return "Commodity{" +
                "id='" + getId() + '\'' +
                ", name='" + name + '\'' +
                ", averagePrice=" + averagePrice +
                ", rare=" + rare +
                '}';
    }

    @Override
    public void readFromStream(EnhancedDataInputStream inputStream) throws IOException {
        setId(inputStream.readIntegerObject());
        setName(inputStream.readString());
        setAveragePrice(inputStream.readLongObject());
        setRare(inputStream.readBooleanObject());
    }

    @Override
    public void writeToStream(EnhancedDataOutputStream outputStream) throws IOException {
        outputStream.writeIntegerObject(getId());
        outputStream.writeString(getName());
        outputStream.writeLongObject(getAveragePrice());
        outputStream.writeBooleanObject(isRare());
    }
}
