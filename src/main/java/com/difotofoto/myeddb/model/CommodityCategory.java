package com.difotofoto.myeddb.model;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class CommodityCategory extends BaseModel {
    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "CommodityCategory{" +
                "name='" + name + '\'' +
                '}';
    }
}
