package com.difotofoto.myeddb.model;

import com.difotofoto.myeddb.io.EnhancedDataInputStream;
import com.difotofoto.myeddb.io.EnhancedDataOutputStream;

import java.io.IOException;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public interface Streamable {
    public void readFromStream(EnhancedDataInputStream inputStream) throws IOException;

    public void writeToStream(EnhancedDataOutputStream outputStream) throws IOException;
}
