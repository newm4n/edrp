package com.difotofoto.myeddb.model;

/**
 * Created by Ferdinand on 3/27/2016.
 */
public enum PowerEnum {

    NONE("None"),
    AISLING_DUVAl("Aisling Duval"),
    ARCHON_DELAINE("Archon Delaine"),
    ARISSA_LAVIGNY_DUVAL("Arissa Lavigny-Duval"),
    DENTON_PATREUS("Denton Patreus"),
    EDMUND_MAHON("Edmund Mahon"),
    FELICIA_WINTERS("Felicia Winters"),
    LI_YONG_RUI("Li Yong-Rui"),
    PRANAV_ANTAL("Pranav Antal"),
    ZACHARY_HUDSON("Zachary Hudson"),
    ZEMINA_TORVAL("Zemina Torval");


    private PowerEnum(String printName) {
        this.printName = printName;
    }

    private String printName;

    public String getPrintName() {
        return printName;
    }

    public static PowerEnum getByPrintName(String printName) {
        for (PowerEnum pe : PowerEnum.values()) {
            if (pe.getPrintName().equals(printName)) return pe;
        }
        return null;
    }
}
