package com.difotofoto.myeddb.model;

import java.util.ArrayList;

/**
 * Created by ferdinand on 3/10/2016.
 */
public class TradeLoop extends ArrayList<Trade> implements Comparable<TradeLoop> {

    public long getTotalProfit() {
        long tot = 0;
        for (Trade t : this) {
            tot += t.getTradeProfit();
        }
        return tot;
    }

    public int compareTo(TradeLoop o) {
        return (int) (o.getTotalProfit() - this.getTotalProfit());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("TOTAL : Cr. " + getTotalProfit() + "\n");
        for (int i = 0; i < size(); i++) {
            sb.append("\t" + (i + 1) + ". " + get(i).toString() + "\n");
        }
        return sb.toString();
    }
}
