package com.difotofoto.myeddb.model;

import com.difotofoto.myeddb.io.EnhancedDataInputStream;
import com.difotofoto.myeddb.io.EnhancedDataOutputStream;

import java.io.IOException;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class Market extends BaseModel implements Streamable, Comparable<Market> {
    private Station station;
    private Commodity commodity;
    private Long buy;
    private Long sell;
    private Integer supply;
    private int demand;

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Long getBuy() {
        return buy;
    }

    public void setBuy(Long buy) {
        this.buy = buy;
    }

    public Long getSell() {
        return sell;
    }

    public void setSell(Long sell) {
        this.sell = sell;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public Integer getSupply() {
        return supply;
    }

    public void setSupply(Integer supply) {
        this.supply = supply;
    }

    public Integer getDemand() {
        return demand;
    }

    public void setDemand(Integer demand) {
        this.demand = demand;
    }

    @Override
    public void readFromStream(EnhancedDataInputStream inputStream) throws IOException {
        setId(inputStream.readIntegerObject());
        setBuy(inputStream.readLongObject());
        setSell(inputStream.readLongObject());
        setCommodity(getGraph().commodities.get(inputStream.readIntegerObject()));
        setSupply(inputStream.readIntegerObject());
        setDemand(inputStream.readIntegerObject());
    }

    @Override
    public void writeToStream(EnhancedDataOutputStream outputStream) throws IOException {
        outputStream.writeIntegerObject(getId());
        outputStream.writeLongObject(getBuy());
        outputStream.writeLongObject(getSell());
        outputStream.writeIntegerObject(getCommodity().getId());
        outputStream.writeIntegerObject(getSupply());
        outputStream.writeIntegerObject(getDemand());
    }

    @Override
    public int compareTo(Market o) {
        return this.getCommodity().getName().compareTo(o.getCommodity().getName());
    }
}
