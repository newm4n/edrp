package com.difotofoto.myeddb.model;

import com.difotofoto.myeddb.importer.serializer.NumericBooleanDeserializer;
import com.difotofoto.myeddb.importer.serializer.NumericBooleanSerializer;
import com.difotofoto.myeddb.io.EnhancedDataInputStream;
import com.difotofoto.myeddb.io.EnhancedDataOutputStream;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ferdinand on 3/10/2016.
 */

/*
{
    "id":17270,
    "name":"Suttundjina",
    "x":107.46875,
    "y":-144.375,
    "z":-11.84375,
    "faction":"Suttundjina Guardians",
    "population":59754,
    "government":"Patronage",
    "allegiance":"Empire",
    "state":null,
    "security":null,
    "primary_economy":null,
    "power":"Arissa Lavigny-Duval",
    "power_state":"Exploited",
    "needs_permit":0,
    "updated_at":1451665542,"simbad_ref":""}
 */

/*
"name":"Gateway","x":-11,"y":77.84375,"z":-0.875,
"name":"Leesti","x":72.75,"y":48.75,"z":68.25
"name":"Robor","x":58.5,"y":64.8125,"z":30.3125
"name":"Meenates","x":-78.71875,"y":53.5625,"z":-23.4375
"name":"37 Librae","x":5.8125,"y":54.375,"z":76.875,
"name":"Kons","x":18.15625,"y":100.90625,"z":-40.21875
*/

@JsonIgnoreProperties(ignoreUnknown = true)
public class StarSystem extends BaseModel implements Streamable, Comparable<StarSystem> {
    @JsonProperty(value = "name")
    private String name;

    @JsonProperty(value = "x")
    private Double x;

    @JsonProperty(value = "y")
    private Double y;

    @JsonProperty(value = "z")
    private Double z;

    @JsonProperty(value = "faction")
    private String faction;

    @JsonProperty(value = "population")
    private Long population;

    @JsonProperty(value = "government")
    private String government;

    @JsonIgnoreProperties
    private GovernmentEnum governmentEnum;

    @JsonProperty(value = "allegiance")
    private String allegiance;

    @JsonIgnoreProperties
    private AllegianceEnum allegianceEnum;

    @JsonProperty(value = "power")
    private String power;

    @JsonIgnoreProperties
    private PowerEnum powerEnum;

    @JsonProperty(value = "needs_permit")
    @JsonSerialize(using = NumericBooleanSerializer.class)
    @JsonDeserialize(using = NumericBooleanDeserializer.class)
    private Boolean needPermit;

    @JsonProperty(value = "power_state")
    private String powerState;

    @JsonIgnoreProperties
    private Boolean hasStation = false;

    @JsonIgnoreProperties
    private Boolean hasStationWithMarket = false;

    private List<Station> stations = new ArrayList<Station>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getX() {
        return x;
    }

    public void setX(Double x) {
        this.x = x;
    }

    public Double getY() {
        return y;
    }

    public void setY(Double y) {
        this.y = y;
    }

    public Double getZ() {
        return z;
    }

    public void setZ(Double z) {
        this.z = z;
    }

    public List<Station> getStations() {
        return stations;
    }

    public void setStations(List<Station> stations) {
        this.stations = stations;
    }

    public String getFaction() {
        return faction;
    }

    public void setFaction(String faction) {
        this.faction = faction;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public String getGovernment() {
        return government;
    }

    public void setGovernment(String government) {
        this.government = government;
        if (government != null) {
            setGovernmentEnum(GovernmentEnum.getByPrintName(government));
        }
    }

    public GovernmentEnum getGovernmentEnum() {
        return governmentEnum;
    }

    public void setGovernmentEnum(GovernmentEnum governmentEnum) {
        this.governmentEnum = governmentEnum;
    }

    public String getAllegiance() {
        return allegiance;
    }

    public void setAllegiance(String allegiance) {
        this.allegiance = allegiance;
        if (allegiance != null) {
            setAllegianceEnum(AllegianceEnum.getByPrintName(allegiance));
        }
    }

    public AllegianceEnum getAllegianceEnum() {
        return allegianceEnum;
    }

    public void setAllegianceEnum(AllegianceEnum allegianceEnum) {
        this.allegianceEnum = allegianceEnum;
    }

    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
        if (power != null) {
            setPowerEnum(PowerEnum.getByPrintName(power));
        }
    }

    public PowerEnum getPowerEnum() {
        return powerEnum;
    }

    public void setPowerEnum(PowerEnum powerEnum) {
        this.powerEnum = powerEnum;
    }

    public Boolean isNeedPermit() {
        if (needPermit == null) return true;
        return needPermit;
    }

    public void setNeedPermit(Boolean needPermit) {
        this.needPermit = needPermit;
    }

    public String getPowerState() {
        return powerState;
    }

    public void setPowerState(String powerState) {
        this.powerState = powerState;
    }

    public Boolean getHasStation() {
        return hasStation;
    }

    public void setHasStation(Boolean hasStation) {
        this.hasStation = hasStation;
    }

    public Boolean getHasStationWithMarket() {
        return hasStationWithMarket;
    }

    public void setHasStationWithMarket(Boolean hasStationWithMarket) {
        this.hasStationWithMarket = hasStationWithMarket;
    }

    public double distanceTo(StarSystem that) {
        double xDif = Math.abs(this.getX() - that.getX());
        double yDif = Math.abs(this.getY() - that.getY());
        double zDif = Math.abs(this.getZ() - that.getZ());
        return Math.sqrt((xDif * xDif) + (yDif * yDif) + (zDif * zDif));
    }

    @Override
    public void readFromStream(EnhancedDataInputStream inputStream) throws IOException {
        setId(inputStream.readIntegerObject());
        setName(inputStream.readString());
        setX(inputStream.readDoubleObject());
        setY(inputStream.readDoubleObject());
        setZ(inputStream.readDoubleObject());
        setFaction(inputStream.readString());
        setPopulation(inputStream.readLongObject());
        setGovernment(inputStream.readString());
        setAllegiance(inputStream.readString());
        setPower(inputStream.readString());
        setNeedPermit(inputStream.readBooleanObject());
        setPowerState(inputStream.readString());
        setHasStation(inputStream.readBooleanObject());
        setHasStationWithMarket(inputStream.readBooleanObject());

        getStations().clear();
        int stationCount = inputStream.readIntegerObject();
        for (int i = 0; i < stationCount; i++) {
            Station station = new Station();
            station.setGraph(getGraph());
            station.readFromStream(inputStream);
            getStations().add(station);
            station.setStarSystem(this);
            getGraph().getStations().put(station.getId(), station);
        }
    }

    @Override
    public void writeToStream(EnhancedDataOutputStream outputStream) throws IOException {
        outputStream.writeIntegerObject(getId());
        outputStream.writeString(getName());
        outputStream.writeDoubleObject(getX());
        outputStream.writeDoubleObject(getY());
        outputStream.writeDoubleObject(getZ());
        outputStream.writeString(getFaction());
        outputStream.writeLongObject(getPopulation());
        outputStream.writeString(getGovernment());
        outputStream.writeString(getAllegiance());
        outputStream.writeString(getPower());
        outputStream.writeBooleanObject(isNeedPermit());
        outputStream.writeString(getPowerState());
        outputStream.writeBooleanObject(getHasStation());
        outputStream.writeBooleanObject(getHasStationWithMarket());

        int stationCount = 0;
        for (Station station : getStations()) {
            if (station.getMarkets().size() > 0)
                stationCount++;
        }

        outputStream.writeIntegerObject(stationCount);

        for (Station station : getStations()) {
            if (station.getMarkets().size() > 0)
                station.writeToStream(outputStream);
        }
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int compareTo(StarSystem that) {
        return this.getName().compareTo(that.getName());
    }
}
