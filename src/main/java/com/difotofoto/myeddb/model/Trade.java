package com.difotofoto.myeddb.model;

import java.text.DecimalFormat;

/**
 * Created by ferdinand on 3/10/2016.
 */
public class Trade implements Comparable<Trade> {
    private Commodity commodity;
    private Station buyStation;
    private Station sellStation;
    private long buyPrice;
    private long sellPrice;

    public long getTradeProfit() {
        return sellPrice - buyPrice;
    }

    public Commodity getCommodity() {
        return commodity;
    }

    public void setCommodity(Commodity commodity) {
        this.commodity = commodity;
    }

    public Station getBuyStation() {
        return buyStation;
    }

    public void setBuyStation(Station buyStation) {
        this.buyStation = buyStation;
    }

    public Station getSellStation() {
        return sellStation;
    }

    public void setSellStation(Station sellStation) {
        this.sellStation = sellStation;
    }

    public long getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(long buyPrice) {
        this.buyPrice = buyPrice;
    }

    public long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(long sellPrice) {
        this.sellPrice = sellPrice;
    }

    public int compareTo(Trade o) {
        return (int) (o.getTradeProfit() - this.getTradeProfit());
    }

    public static DecimalFormat df = new DecimalFormat("#,###,###,##0.00");
    public static DecimalFormat dff = new DecimalFormat("#,###,###,##0");

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Cr." + getTradeProfit() + " profit. ");
        sb.append("Buy " + commodity.getName());
        sb.append(" at [" + buyStation.getName());
        sb.append("@" + buyStation.getStarSystem().getName() + " [" + buyStation.getMaxLandingPadSize() + "] " + dff.format(buyStation.getDistanceToStar()) + "ls]");
        sb.append(" for Cr." + buyPrice);
        sb.append(" & Sell at [" + sellStation.getName());
        sb.append("@" + sellStation.getStarSystem().getName() + " [" + sellStation.getMaxLandingPadSize() + "] " + dff.format(sellStation.getDistanceToStar()) + "ls]");
        sb.append(" for Cr." + sellPrice);
        sb.append(" Dist:" + df.format(buyStation.getStarSystem().distanceTo(sellStation.getStarSystem())) + "ly");
        return sb.toString();
    }
}
