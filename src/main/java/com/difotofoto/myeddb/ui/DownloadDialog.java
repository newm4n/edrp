package com.difotofoto.myeddb.ui;

import com.difotofoto.myeddb.Graph;
import com.difotofoto.myeddb.importer.DownloadProgressListener;
import com.difotofoto.myeddb.importer.HttpImporter;
import com.difotofoto.myeddb.model.Commodity;
import com.difotofoto.myeddb.model.Market;
import com.difotofoto.myeddb.model.StarSystem;
import com.difotofoto.myeddb.model.Station;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.log4j.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public class DownloadDialog extends JDialog {

    Logger log = Logger.getLogger(DownloadDialog.class);

    public static String COMMODITIES_JSON_URL = "https://eddb.io/archive/v4/commodities.json";
    public static String SYSTEM_JSON_URL = "https://eddb.io/archive/v4/systems.json";
    public static String STATION_JSON_URL = "https://eddb.io/archive/v4/stations.json";
    public static String LISTINGS_CSV_URL = "https://eddb.io/archive/v4/listings.csv";

    JProgressBar allDownloadProgress = new JProgressBar();
    JProgressBar commodityDownloadProgress = new JProgressBar();
    JProgressBar systemDownloadProgress = new JProgressBar();
    JProgressBar stationDownloadProgress = new JProgressBar();
    JProgressBar marketDownloadProgress = new JProgressBar();
    JLabel lbAll = new JLabel("All");
    JLabel lbCommodity = new JLabel("Commodity");
    JLabel lbSystem = new JLabel("System");
    JLabel lbStation = new JLabel("Station");
    JLabel lbTrades = new JLabel("Trade Route");

    JButton pbCancel = new JButton("Cancel");

    public boolean success = false;

    public DownloadDialog() {
        this.setTitle("Loading from EDDB");
        JPanel panel = new JPanel(new GridBagLayout());
        this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        this.setModal(true);

        allDownloadProgress.setPreferredSize(new Dimension(200, 18));
        commodityDownloadProgress.setPreferredSize(new Dimension(200, 18));
        systemDownloadProgress.setPreferredSize(new Dimension(200, 18));
        stationDownloadProgress.setPreferredSize(new Dimension(200, 18));
        marketDownloadProgress.setPreferredSize(new Dimension(200, 18));

        allDownloadProgress.setStringPainted(false);
        commodityDownloadProgress.setStringPainted(true);
        systemDownloadProgress.setStringPainted(true);
        stationDownloadProgress.setStringPainted(true);
        marketDownloadProgress.setStringPainted(true);

        lbAll.setPreferredSize(new Dimension(100, 18));
        lbCommodity.setPreferredSize(new Dimension(100, 18));
        lbSystem.setPreferredSize(new Dimension(100, 18));
        lbStation.setPreferredSize(new Dimension(100, 18));
        lbTrades.setPreferredSize(new Dimension(100, 18));

        GridBagConstraints ct = new GridBagConstraints();
        ct.insets = new Insets(5, 5, 5, 5);
        ct.gridx = 0;
        ct.gridy = 0;
        panel.add(lbAll, ct);
        ct.gridx = 0;
        ct.gridy = 1;
        panel.add(lbSystem, ct);
        ct.gridx = 0;
        ct.gridy = 2;
        panel.add(lbStation, ct);
        ct.gridx = 0;
        ct.gridy = 3;
        panel.add(lbCommodity, ct);
        ct.gridx = 0;
        ct.gridy = 4;
        panel.add(lbTrades, ct);

        ct.gridx = 1;
        ct.gridy = 0;
        panel.add(allDownloadProgress, ct);
        ct.gridx = 1;
        ct.gridy = 1;
        panel.add(systemDownloadProgress, ct);
        ct.gridx = 1;
        ct.gridy = 2;
        panel.add(stationDownloadProgress, ct);
        ct.gridx = 1;
        ct.gridy = 3;
        panel.add(commodityDownloadProgress, ct);
        ct.gridx = 1;
        ct.gridy = 4;
        panel.add(marketDownloadProgress, ct);
        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel buttonPanel = new JPanel(new FlowLayout());
        buttonPanel.add(pbCancel);
        buttonPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JPanel mainPanel = new JPanel(new BorderLayout());
        this.setContentPane(mainPanel);
        mainPanel.add(panel, BorderLayout.CENTER);
        // mainPanel.add(buttonPanel, BorderLayout.SOUTH);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowOpened(WindowEvent e) {
                SwingUtilities.invokeLater(() -> loadThread.start());
            }
        });

        pbCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(DownloadDialog.this, "Cancel");
            }
        });

        this.pack();
        FrameUtil.center(this);
    }

    private Thread loadThread = new Thread(new Runnable() {
        @Override
        public void run() {
            load();
        }
    });

    public Map<Integer, StarSystem> systems = new HashMap<Integer, StarSystem>();
    public Map<Integer, Station> stations = new HashMap<Integer, Station>();
    public Map<Integer, Commodity> commodities = new HashMap<Integer, Commodity>();
    public Map<Integer, Market> markets = new HashMap<Integer, Market>();

    public Graph getGraph() {
        Graph g = new Graph();
        g.setGraphDate(new Date());
        g.setCommodities(commodities);
        g.setMarkets(markets);
        g.setStations(stations);
        g.setSystems(systems);
        return g;
    }

    public void load() {
        log.info("Start Loading");
        systems.clear();
        stations.clear();
        commodities.clear();
        markets.clear();

        allDownloadProgress.setMaximum(4);
        allDownloadProgress.setMinimum(0);
        allDownloadProgress.setValue(0);

        long start = System.currentTimeMillis();
        try {
            importSystem();
            allDownloadProgress.setValue(1);
            importStation();
            allDownloadProgress.setValue(2);

            for (Station station : stations.values()) {
                StarSystem ss = systems.get(station.getSystemId());
                if (ss != null) {
                    station.setStarSystem(ss);
                    ss.getStations().add(station);
                    ss.setHasStation(true);
                } else {
                    log.warn(station.getName() + " has unknown star system");
                }
            }

            importCommodity();
            allDownloadProgress.setValue(3);
            importMarket();
            allDownloadProgress.setValue(4);
            success = true;
        } catch (IOException e) {
            e.printStackTrace();
            success = false;
        }
        log.info("Finish import in " + (System.currentTimeMillis() - start) + " ms");
        this.setVisible(false);
    }


    public void importCommodity() throws IOException {
        log.info("Downloading Commodity");
        InputStream is = new ByteArrayInputStream(HttpImporter.fetchV1(COMMODITIES_JSON_URL,
                new ProgressBarDownloadProgressListener(commodityDownloadProgress)));
        ObjectMapper objectMapper = new ObjectMapper();
        java.util.List<Commodity> commodityList = objectMapper.readValue(is,
                TypeFactory.defaultInstance().constructCollectionType(java.util.List.class,
                        Commodity.class));
        for (Commodity c : commodityList) {
            commodities.put(c.getId(), c);
        }
    }

    public void importSystem() throws IOException {
        log.info("Downloading System");
        InputStream is = new ByteArrayInputStream(HttpImporter.fetchV1(SYSTEM_JSON_URL,
                new ProgressBarDownloadProgressListener(systemDownloadProgress)));
        ObjectMapper objectMapper = new ObjectMapper();
        java.util.List<StarSystem> systemList = objectMapper.readValue(is,
                TypeFactory.defaultInstance().constructCollectionType(java.util.List.class,
                        StarSystem.class));
        for (StarSystem c : systemList) {
            systems.put(c.getId(), c);
        }
    }

    public void importStation() throws IOException {
        log.info("Downloading Station");
        InputStream is = new ByteArrayInputStream(HttpImporter.fetchV1(STATION_JSON_URL,
                new ProgressBarDownloadProgressListener(stationDownloadProgress)));
        ObjectMapper objectMapper = new ObjectMapper();
        java.util.List<Station> stationList = objectMapper.readValue(is,
                TypeFactory.defaultInstance().constructCollectionType(java.util.List.class,
                        Station.class));
        for (Station c : stationList) {
            stations.put(c.getId(), c);
        }
    }

    public void importMarket() throws IOException {
        log.info("Downloading Market");
        InputStream is = new ByteArrayInputStream(HttpImporter.fetchV1(LISTINGS_CSV_URL,
                new ProgressBarDownloadProgressListener(marketDownloadProgress)));
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = br.readLine();
        while (true) {
            line = br.readLine();
            if (line == null) break;
            String[] split = line.split(",");
            Market m = new Market();
            m.setSupply(Integer.parseInt(split[3]));
            m.setBuy(Long.parseLong(split[4]));
            m.setSell(Long.parseLong(split[5]));
            m.setDemand(Integer.parseInt(split[6]));
            Station station = stations.get(Integer.parseInt(split[1]));
            if (station != null) {
                m.setStation(station);
                station.getMarkets().add(m);
                station.getStarSystem().setHasStationWithMarket(true);
            } else {
                System.out.println("Market ID : " + m.getId() + " have invalid station id " + split[1]);
            }
            m.setId(Integer.parseInt(split[0]));
            Commodity commodity = commodities.get(Integer.parseInt(split[2]));
            if (commodity != null) {
                m.setCommodity(commodity);
            } else {
                System.out.println("Market ID : " + m.getId() + " have invalid commodity id " + split[2]);
            }
            markets.put(m.getId(), m);
        }
    }


    class ProgressBarDownloadProgressListener implements DownloadProgressListener {
        private JProgressBar progressBar;

        private int percentage;
        private long length = -1;

        public ProgressBarDownloadProgressListener(JProgressBar progressBar) {
            this.progressBar = progressBar;
            this.progressBar.setMinimum(0);
            this.progressBar.setMaximum(100);
        }

        @Override
        public void contentLength(long length) {
            this.length = length;
        }

        @Override
        public void start() {
            if (length > 0) {
                progressBar.setIndeterminate(false);
            } else {
                progressBar.setIndeterminate(true);
            }
        }

        @Override
        public void finish() {

        }

        @Override
        public void downloaded(long downloaded) {
            double dl = (((double) downloaded) / ((double) length)) * 100d;
            percentage = (int) Math.round(dl);
            progressBar.setValue(percentage);
            progressBar.setString(percentage + "%");
        }
    }
}
