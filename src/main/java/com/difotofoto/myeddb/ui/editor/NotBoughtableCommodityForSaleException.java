package com.difotofoto.myeddb.ui.editor;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class NotBoughtableCommodityForSaleException extends Exception {
    public NotBoughtableCommodityForSaleException(String message) {
        super(message);
    }
}
