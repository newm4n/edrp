package com.difotofoto.myeddb.ui.editor;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class NoTradableCommodityException extends Exception {
    public NoTradableCommodityException(String message) {
        super(message);
    }
}
