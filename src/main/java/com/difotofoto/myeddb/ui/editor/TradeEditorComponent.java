package com.difotofoto.myeddb.ui.editor;

import com.difotofoto.myeddb.Constants;
import com.difotofoto.myeddb.model.Market;
import com.difotofoto.myeddb.model.Station;
import com.difotofoto.myeddb.model.TradeLoop;
import com.difotofoto.myeddb.ui.MainFrame;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class TradeEditorComponent extends JComponent implements MouseListener, MouseMotionListener {

    private List<TradeRouteNode> routeNodes = new ArrayList<>();
    private List<TradeRoute> tradeRoutes = new ArrayList<>();

    JPopupMenu editorComponentPopup = new JPopupMenu("StarSystem");
    JMenuItem systemInfo = new JMenuItem("System Info");
    JMenuItem removeLastSystem = new JMenuItem("Remove Last System");
    JMenuItem clear = new JMenuItem("Clear");
    JMenuItem saveDiagram = new JMenuItem("Save Diagram");

    public TradeEditorComponent() {
        this.setOpaque(true);

        this.addMouseListener(this);
        this.addMouseMotionListener(this);

        editorComponentPopup.add(systemInfo);
        editorComponentPopup.add(new JPopupMenu.Separator());
        editorComponentPopup.add(removeLastSystem);
        editorComponentPopup.add(clear);
        editorComponentPopup.add(new JPopupMenu.Separator());
        editorComponentPopup.add(saveDiagram);

        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (JOptionPane.showConfirmDialog(TradeEditorComponent.this, "Are you sure to clear the diagram ?", "Clear", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                    routeNodes.clear();
                    tradeRoutes.clear();
                }
                TradeEditorComponent.this.repaint();
            }
        });

        removeLastSystem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(routeNodes.size() > 1) {
                    if(!routeNodes.get(routeNodes.size()-2).getLockOn().equals(TradeRouteNode.LockOn.COMMODITY)) {
                        routeNodes.get(routeNodes.size()-2).setCommodityToBuy(null);
                        routeNodes.get(routeNodes.size()-2).setBuyPrice(null);
                    }
                }
                routeNodes.remove(routeNodes.size() - 1);
                if (routeNodes.size() > 0) {
                    tradeRoutes.remove(tradeRoutes.size() - 1);
                }
                TradeEditorComponent.this.repaint();
            }
        });

        saveDiagram.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "PNG Image", "png");
                chooser.setFileFilter(filter);

                int returnVal = chooser.showOpenDialog(TradeEditorComponent.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {

                    File choosen = chooser.getSelectedFile();
                    if (choosen.getName().toUpperCase().endsWith(".PNG") == false) {
                        choosen = new File(choosen.getAbsolutePath() + ".png");
                    }

                    if (choosen.exists()) {
                        if (choosen.isFile()) {
                            if (JOptionPane.showConfirmDialog(TradeEditorComponent.this, "File " + choosen.getName() + " is exist. Overwrite ?", "Save Diagram", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                                choosen.delete();
                            } else {
                                return;
                            }
                        } else {
                            JOptionPane.showMessageDialog(TradeEditorComponent.this, "Can not save to " + choosen.getAbsolutePath() + ". Its a directory.", "Save diagram", JOptionPane.ERROR_MESSAGE);
                            return;
                        }
                    }

                    BufferedImage image = new BufferedImage(TradeEditorComponent.this.getSize().width, TradeEditorComponent.this.getSize().height, BufferedImage.TYPE_INT_ARGB);
                    Graphics2D g2d = image.createGraphics();
                    TradeEditorComponent.this.paintComponent(g2d);
                    try {
                        ImageIO.write(image, "png", choosen);
                    } catch (IOException e1) {
                        JOptionPane.showMessageDialog(TradeEditorComponent.this, "Error during saving diagram into file : " + e1.getMessage(), "Save diagram", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });


        this.setComponentPopupMenu(editorComponentPopup);
        editorComponentPopup.addPopupMenuListener(new PopupMenuListener() {
            @Override
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                systemInfo.setEnabled(false);
                removeLastSystem.setEnabled(false);
                clear.setEnabled(false);
                saveDiagram.setEnabled(false);
                if (routeNodes.size() > 0) {
                    clear.setEnabled(true);
                    saveDiagram.setEnabled(true);
                }
                for (TradeRouteNode node : routeNodes) {
                    if (node.getRectangle().contains(TradeEditorComponent.this.getMousePosition())) {
                        nodeOnRightClick = node;
                        systemInfo.setEnabled(true);
                        removeLastSystem.setEnabled(false);
                        if (node.equals(routeNodes.get(routeNodes.size() - 1))) {
                            removeLastSystem.setEnabled(true);
                        }
                        return;
                    }
                }
            }

            @Override
            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            @Override
            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });
    }

    public void addTradeRouteNode(TradeRouteNode node) {
        if (routeNodes.size() == 0) {
            routeNodes.add(node);
            node.setLocation(new Point((int) ((this.getSize().getWidth() / 2) - (node.getSize().getWidth() / 2)), 20));
        } else {
            TradeRouteNode lastNode = routeNodes.get(routeNodes.size() - 1);
            TradeRoute tradeRoute = null;
            try {
                tradeRoute = makeTrade(lastNode, node);
                if (tradeRoute != null) {
                    routeNodes.add(node);
                    node.setLocation(new Point((int) ((this.getSize().getWidth() / 2) - (node.getSize().getWidth() / 2)), 20));
                    tradeRoutes.add(tradeRoute);
                } else {
                    JOptionPane.showMessageDialog(this, "This is strange, no tradeRoute combinations can be calculated from the last tradeRoute node to the node you've just added.", "Add Node", JOptionPane.ERROR_MESSAGE);
                }
            } catch (NoTradableCommodityException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Add Node", JOptionPane.ERROR_MESSAGE);
            } catch (IncompatibleCommodityForTradingException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Add Node", JOptionPane.ERROR_MESSAGE);
            } catch (LostTradingException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Add Node", JOptionPane.ERROR_MESSAGE);
            } catch (NotSellableBoughtCommodityException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Add Node", JOptionPane.ERROR_MESSAGE);
            } catch (NotBoughtableCommodityForSaleException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Add Node", JOptionPane.ERROR_MESSAGE);
            } catch (SellAndBuySameStationException e) {
                JOptionPane.showMessageDialog(this, e.getMessage(), "Add Node", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public TradeRoute makeTrade(TradeRouteNode source, TradeRouteNode target) throws SellAndBuySameStationException, NoTradableCommodityException, IncompatibleCommodityForTradingException, LostTradingException, NotSellableBoughtCommodityException, NotBoughtableCommodityForSaleException {
        // Compare if both Commodity is specified.
        if (source.getCommodityToBuy() != null &&
                target.getCommodityToSell() != null &&
                source.getCommodityToBuy().equals(target.getCommodityToSell())) {
            if (source.getStation().equals(target.getStation())) {
                throw new SellAndBuySameStationException("Buying and Selling a commodity in the same place usually yield loss or pointless");
            } else {
                return new TradeRoute(source, target);
            }
        } else if (source.getCommodityToBuy() != null &&
                target.getCommodityToSell() != null &&
                source.getCommodityToBuy().equals(target.getCommodityToSell()) == false) {
            throw new IncompatibleCommodityForTradingException("Commodity " + source.getCommodityToBuy().getName() + " bought on " + source.getStation().getName() + " can't be sold as " + target.getCommodityToSell().getName() + " at " + target.getStation().getName());
        }

        // Compare if the source specified Commodity while the target specified station
        else if (source.getCommodityToBuy() != null && target.getCommodityToSell() == null && target.getStation() != null) {
            // check if the target has commodity at target can be sold at the destination station
            for (Market market : target.getStation().getMarkets()) {
                if (market.getCommodity().equals(source.getCommodityToBuy())) {
                    if (market.getSell() - source.getBuyPrice() > 0) {
                        target.setCommodityToSell(source.getCommodityToBuy());
                        target.setSellPrice(market.getSell());
                        target.setDemand(market.getDemand());
                        return new TradeRoute(source, target);
                    } else {
                        throw new LostTradingException("Commodity " + source.getCommodityToBuy().getName() + " bought at " + source.getStation().getName() + " making lost if sold at " + target.getStation().getName());
                    }
                }
            }
            throw new NotSellableBoughtCommodityException("Commodity " + source.getCommodityToBuy().getName() + " bought at " + source.getStation().getName() + " can not be sold at " + target.getStation().getName());
        }

        // Compare if the source specified station while the target specified commodity
        else if (source.getCommodityToBuy() == null && source.getStation() != null && target.getCommodityToSell() != null) {
            // check if the source has commodity at source can be bought at the source station
            for (Market market : source.getStation().getMarkets()) {
                if (market.getCommodity().equals(target.getCommodityToSell())) {
                    if (source.getSellPrice() - market.getBuy() > 0) {
                        source.setCommodityToBuy(target.getCommodityToSell());
                        source.setBuyPrice(market.getBuy());
                        source.setSupply(market.getSupply());
                        return new TradeRoute(source, target);
                    } else {
                        throw new LostTradingException("Commodity " + target.getCommodityToSell().getName() + " sold at " + target.getStation().getName() + " making lost if bought at " + source.getStation().getName());
                    }
                }
            }
            throw new NotSellableBoughtCommodityException("Commodity " + target.getCommodityToSell().getName() + " sold at " + target.getStation().getName() + " can not be bought at " + source.getStation().getName());
        }

        // Compare if the source specified Commodity while the target specified system
        else if (source.getCommodityToBuy() != null && target.getCommodityToSell() == null && target.getStation() == null && target.getStarSystem() != null) {
            // gather all market in all station within the system that accepting commodity bought at the source
            List<Market> markets = new ArrayList<>();
            for (Station stations : target.getStarSystem().getStations()) {
                Market m = stations.getMarketForCommodity(source.getCommodityToBuy());
                if (m != null) markets.add(m);
            }

            if (markets.size() == 0) {
                throw new NotSellableBoughtCommodityException("Commodity " + source.getCommodityToBuy() + " are not accepted in any station in the " + target.getStarSystem().getName() + " system");
            }

            // Get the highest market.
            Market highest = null;
            for (Market m : markets) {
                if (highest == null) {
                    highest = m;
                } else {
                    if (m.getSell() > highest.getSell()) {
                        highest = m;
                    }
                }
            }

            if (source.getBuyPrice() > highest.getSell()) {
                throw new LostTradingException("Selling commodity " + source.getCommodityToBuy() + " in " + target.getStarSystem().getName() + " system is not bringing any profit");
            }

            target.setStation(highest.getStation());
            target.setCommodityToSell(highest.getCommodity());
            target.setSellPrice(highest.getSell());
            target.setDemand(highest.getDemand());

            return new TradeRoute(source, target);
        }

        // Compare if the source specified system while the target specified commodity
        else if (source.getCommodityToBuy() == null && source.getStation() == null && source.getStarSystem() != null && target.getCommodityToSell() != null) {
            // gather all market in all station within the system that selling commodity sold at the source
            List<Market> markets = new ArrayList<>();
            for (Station stations : source.getStarSystem().getStations()) {
                Market m = stations.getMarketForCommodity(target.getCommodityToSell());
                if (m != null) markets.add(m);
            }

            if (markets.size() == 0) {
                throw new NotBoughtableCommodityForSaleException("Commodity " + target.getCommodityToSell() + " are can not be bought in any station in the " + source.getStarSystem().getName() + " system");
            }

            // Get the lowest market.
            Market lowest = null;
            for (Market m : markets) {
                if (lowest == null) {
                    lowest = m;
                } else {
                    if (m.getBuy() < lowest.getBuy()) {
                        lowest = m;
                    }
                }
            }

            if (target.getSellPrice() < lowest.getBuy()) {
                throw new LostTradingException("Buying commodity " + target.getCommodityToSell() + " in " + source.getStarSystem().getName() + " system is not bringing any profit");
            }

            source.setCommodityToBuy(lowest.getCommodity());
            source.setBuyPrice(lowest.getBuy());
            source.setSupply(lowest.getSupply());

            return new TradeRoute(source, target);
        }

        // Compare if the source specified Station and target specified station
        else if (source.getCommodityToBuy() == null && source.getStation() != null && target.getCommodityToSell() == null && target.getStation() != null) {
            if (source.getStation().equals(target.getStation())) {
                throw new SellAndBuySameStationException("Buying and Selling a commodity in the same place usually yield loss or pointless");
            }
            TradeLoop loop = bestTradeFromStationToStation(source.getStation(), target.getStation());
            if (loop == null) {
                throw new NoTradableCommodityException("No commodity can be trade between station " + source.getStation().getName() + " and station " + target.getStation().getName());
            } else {
                if (loop.getTotalProfit() > 0) {
                    if (loop.get(0).getBuyStation().equals(source.getStation())) {
                        source.setCommodityToBuy(loop.get(0).getCommodity());
                        source.setBuyPrice(loop.get(0).getBuyPrice());
                        target.setCommodityToSell(loop.get(0).getCommodity());
                        target.setSellPrice(loop.get(0).getSellPrice());
                        return new TradeRoute(source, target);
                    } else if (loop.get(1).getBuyStation().equals(source.getStation())) {
                        source.setCommodityToBuy(loop.get(1).getCommodity());
                        source.setBuyPrice(loop.get(1).getBuyPrice());
                        target.setCommodityToSell(loop.get(1).getCommodity());
                        target.setSellPrice(loop.get(1).getSellPrice());
                        return new TradeRoute(source, target);
                    } else {
                        throw new NoTradableCommodityException("No commodity can be trade between station " + source.getStation().getName() + " and station " + target.getStation().getName());
                    }
                } else {
                    throw new LostTradingException("No commodity can be trade between station " + source.getStation().getName() + " and station " + target.getStation().getName());
                }
            }
        }

        // Compare if the source specified station and target specified system
        else if (source.getCommodityToBuy() == null && source.getStation() != null && target.getCommodityToSell() == null && target.getStation() == null && target.getStarSystem() != null) {
            List<Station> groupA = new ArrayList<>();
            groupA.add(source.getStation());
            List<Station> groupB = target.getStarSystem().getStations();

            TradeLoop tl = bestTradeFromStationGroupToStationGroup(groupA, groupB);
            if (tl != null) {
                source.setCommodityToBuy(tl.get(0).getCommodity());
                source.setBuyPrice(tl.get(0).getBuyPrice());
                target.setStation(tl.get(0).getSellStation());
                target.setCommodityToSell(tl.get(0).getCommodity());
                target.setSellPrice(tl.get(0).getSellPrice());
                return new TradeRoute(source, target);
            } else {
                throw new NoTradableCommodityException("No profitable commodity(s) can be bought from station " + source.getStation().getName() + " and sold to any stations in " + target.getStarSystem().getName());
            }
        }

        // Compare if the source specified system and target specified station
        else if (source.getCommodityToBuy() == null && source.getStation() == null && source.getStarSystem() != null && target.getCommodityToSell() == null && target.getStation() != null) {
            List<Station> groupA = source.getStarSystem().getStations();
            List<Station> groupB = new ArrayList<>();
            groupB.add(target.getStation());

            TradeLoop tl = bestTradeFromStationGroupToStationGroup(groupA, groupB);
            if (tl != null) {
                source.setCommodityToBuy(tl.get(0).getCommodity());
                source.setBuyPrice(tl.get(0).getBuyPrice());
                source.setStation(tl.get(0).getBuyStation());
                target.setCommodityToSell(tl.get(0).getCommodity());
                target.setSellPrice(tl.get(0).getSellPrice());
                return new TradeRoute(source, target);
            } else {
                throw new NoTradableCommodityException("No profitable commodity(s) can be bought from any stations in " + source.getStarSystem().getName() + " and sold to station " + target.getStation().getName());
            }
        }

        // Compare if the source specified system and target specified system
        else if (source.getCommodityToBuy() == null && source.getStation() == null && source.getStarSystem() != null && target.getCommodityToSell() == null && target.getStation() == null && target.getStarSystem() != null) {
            List<Station> groupA = source.getStarSystem().getStations();
            List<Station> groupB = target.getStarSystem().getStations();

            TradeLoop tl = bestTradeFromStationGroupToStationGroup(groupA, groupB);
            if (tl != null) {
                source.setStation(tl.get(0).getBuyStation());
                source.setCommodityToBuy(tl.get(0).getCommodity());
                source.setBuyPrice(tl.get(0).getBuyPrice());
                target.setStation(tl.get(0).getSellStation());
                target.setCommodityToSell(tl.get(0).getCommodity());
                target.setSellPrice(tl.get(0).getSellPrice());
                return new TradeRoute(source, target);
            } else {
                throw new NoTradableCommodityException("No profitable commodity(s) can be bought from any stations in " + source.getStarSystem().getName() + " and sold to any stations in " + target.getStarSystem().getName());
            }
        }
        return null;
    }

    public static TradeLoop bestTradeFromStationGroupToStationGroup(List<Station> groupA, List<Station> groupB) {
        List<TradeLoop> tradeRoutes = new ArrayList<TradeLoop>();
        for (Station sa : groupA) {
            for (Station sb : groupB) {
                TradeLoop loop = bestTradeFromStationToStation(sa, sb);
                if (loop != null) {
                    tradeRoutes.add(loop);
                }
            }
        }
        if (tradeRoutes.size() > 0) {
            Collections.sort(tradeRoutes);
            return tradeRoutes.get(0);
        } else {
            return null;
        }
    }

    public static TradeLoop bestTradeFromStationToStation(Station a, Station b) {
        com.difotofoto.myeddb.model.Trade abMost = null;
        long abMax = 0;

        for (Market ma : a.getMarkets()) {
            Market mb = b.getMarketForCommodity(ma.getCommodity());
            if (mb != null) {
                if (ma.getBuy() > 0) {
                    com.difotofoto.myeddb.model.Trade ab = new com.difotofoto.myeddb.model.Trade();
                    ab.setBuyPrice(ma.getBuy());
                    ab.setSellPrice(mb.getSell());
                    ab.setBuyStation(a);
                    ab.setSellStation(b);
                    ab.setCommodity(ma.getCommodity());
                    if (abMost == null) {
                        abMost = ab;
                        abMax = ab.getTradeProfit();
                    } else {
                        long abm = ab.getTradeProfit();
                        if (abm > abMax) {
                            abMost = ab;
                            abMax = abm;
                        }
                    }
                }
            }
        }
        if (abMost != null) {
            TradeLoop loop = new TradeLoop();
            loop.add(abMost);
            return loop;
        } else {
            return null;
        }
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

        g2d.setColor(Color.BLACK);
        g2d.fillRect(0, 0, getSize().width, getSize().height);

        g2d.setColor(Constants.ED_ORANGE);
        g2d.setFont(MainFrame.EUROCAPS_FONT.deriveFont(20f));

        if (tradeRoutes.size() > 0) {


            long tot = 0;
            double totDis = 0;
            for (TradeRoute tr : tradeRoutes) {
                tot += tr.getProfit();
                totDis += tr.getDistance();
            }

            DecimalFormat format = new DecimalFormat("###,###,#00.0");
            DecimalFormat money = new DecimalFormat("###,###,##0");

            String totProfit = "Cr " + money.format(tot) + " Profit / Tons";
            String totDistance = format.format(totDis) + " LY";
            FontMetrics fm = g2d.getFontMetrics();
            g2d.drawString(totProfit, (int) (this.getSize().getWidth() - fm.stringWidth(totProfit) - 20), fm.getHeight());
            g2d.drawString(totDistance, (int) (this.getSize().getWidth() - fm.stringWidth(totDistance) - 20), fm.getHeight() * 2);
        }

        for (int i = 0; i < tradeRoutes.size(); i++) {
            TradeRoute tradeRoute = tradeRoutes.get(i);
            tradeRoute.paint(g);
        }

        for (int i = 0; i < routeNodes.size(); i++) {
            TradeRouteNode node = routeNodes.get(i);
            if (i == 0) {
                node.paint(g, TradeRouteNode.NodeType.Begin);
            } else if (i == routeNodes.size() - 1) {
                node.paint(g, TradeRouteNode.NodeType.End);
            } else {
                node.paint(g, TradeRouteNode.NodeType.Middle);
            }
        }

    }

    TradeRouteNode selectedNode = null;
    Point startPoint = null;
    Point postOffset = null;
    boolean moved = false;

    TradeRouteNode nodeOnRightClick = null;

    @Override
    public void mouseClicked(MouseEvent e) {
//
    }

    @Override
    public void mousePressed(MouseEvent e) {
        for (int i = routeNodes.size() - 1; i >= 0; i--) {
            TradeRouteNode node = routeNodes.get(i);
            if (node.getRectangle().contains(e.getPoint())) {
                selectedNode = node;
                startPoint = e.getPoint();
                postOffset = new Point(startPoint.x - selectedNode.getLocation().x, startPoint.y - selectedNode.getLocation().y);
                moved = false;
                break;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        selectedNode = null;
    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if (selectedNode != null) {
            selectedNode.setLocation(new Point(e.getPoint().x - postOffset.x, e.getPoint().y - postOffset.y));
            this.repaint();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }

    public List<TradeRoute> getTradeRoutes() {
        return tradeRoutes;
    }
}
