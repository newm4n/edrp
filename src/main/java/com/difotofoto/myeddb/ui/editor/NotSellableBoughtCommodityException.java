package com.difotofoto.myeddb.ui.editor;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class NotSellableBoughtCommodityException extends Exception {
    public NotSellableBoughtCommodityException(String message) {
        super(message);
    }
}
