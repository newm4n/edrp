package com.difotofoto.myeddb.ui.editor;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class IncompatibleCommodityForTradingException extends Exception {
    public IncompatibleCommodityForTradingException(String message) {
        super(message);
    }
}
