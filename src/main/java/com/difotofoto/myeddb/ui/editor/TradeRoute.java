package com.difotofoto.myeddb.ui.editor;

import com.difotofoto.myeddb.Constants;
import com.difotofoto.myeddb.ui.MainFrame;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;
import java.text.DecimalFormat;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class TradeRoute {
    TradeRouteNode source;
    TradeRouteNode destination;
    Double distance;
    Long profit;

    public String getDistanceText() {
        DecimalFormat dm = new DecimalFormat("###,###,#00.0");
        return dm.format(distance);
    }

    public TradeRoute(TradeRouteNode source, TradeRouteNode destination) {
        this.source = source;
        this.destination = destination;
        distance = source.getStarSystem().distanceTo(destination.getStarSystem());
        profit = destination.getSellPrice() - source.getBuyPrice();
    }

    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(Constants.ED_ORANGE);

        Rectangle sRec = getSource().getRectangle();
        Rectangle tRec = getDestination().getRectangle();
        Point sCenter = new Point((int) sRec.getCenterX(), (int) sRec.getCenterY());
        Point tCenter = new Point((int) tRec.getCenterX(), (int) tRec.getCenterY());

        Point mid = getMidPoint(sCenter, tCenter);
        g2d.fillOval(mid.x - 40, mid.y - 40, 80, 80);

        GeneralPath p = new GeneralPath();
        p.moveTo(17, 37);
        p.lineTo(17, -37);
        p.lineTo(80, 0);
        p.closePath();
        p.transform(AffineTransform.getRotateInstance(Math.toRadians(getAngle(sCenter, tCenter))));
        p.transform(AffineTransform.getTranslateInstance(mid.getX(), mid.getY()));

        g2d.fill(p);

        Stroke originalStroke = g2d.getStroke();
        g2d.setStroke(new BasicStroke(4f));
        g2d.drawLine((int) sRec.getCenterX(), (int) sRec.getCenterY(), (int) tRec.getCenterX(), (int) tRec.getCenterY());
        g2d.setStroke(originalStroke);

        g2d.setFont(MainFrame.EUROCAPS_FONT.deriveFont(14f));
        g2d.setColor(Color.BLACK);

        DecimalFormat format = new DecimalFormat("###,###,#00.0");
        String distance = format.format(getDistance()) + " ly";

        DecimalFormat money = new DecimalFormat("###,###,##0");
        String profit = "Cr " + money.format(getProfit());

        FontMetrics fm = g2d.getFontMetrics();

        g2d.drawString(profit, (int) (mid.getX() - (fm.stringWidth(profit) / 2)), (int) (mid.getY() - 3));
        g2d.drawString(distance, (int) (mid.getX() - (fm.stringWidth(distance) / 2)), (int) (mid.getY() + fm.getHeight()));
    }

    public double getAngle(Point source, Point target) {
        double angle = Math.toDegrees(Math.atan2(target.y - source.y, target.x - source.x));
        if (angle < 0) {
            angle += 360;
        }
        return angle;
    }

    public Point getMidPoint(Point source, Point target) {
        int x = (source.x + target.x) / 2;
        int y = (source.y + target.y) / 2;
        return new Point(x, y);
    }

    public TradeRouteNode getSource() {
        return source;
    }

    public void setSource(TradeRouteNode source) {
        this.source = source;
    }

    public TradeRouteNode getDestination() {
        return destination;
    }

    public void setDestination(TradeRouteNode destination) {
        this.destination = destination;
    }

    public long getProfit() {
        return destination.getSellPrice() - source.getBuyPrice();
    }

    public double getDistance() {
        return source.getStarSystem().distanceTo(destination.getStarSystem());
    }
}
