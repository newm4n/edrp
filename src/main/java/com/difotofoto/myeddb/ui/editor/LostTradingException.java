package com.difotofoto.myeddb.ui.editor;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class LostTradingException extends Exception {
    public LostTradingException(String message) {
        super(message);
    }
}
