package com.difotofoto.myeddb.ui.editor;

/**
 * Created by Ferdinand on 4/29/2016.
 */
public class SellAndBuySameStationException extends Exception {
    public SellAndBuySameStationException(String message) {
        super(message);
    }
}