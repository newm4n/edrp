package com.difotofoto.myeddb.ui.editor;

import com.difotofoto.myeddb.Constants;
import com.difotofoto.myeddb.model.Commodity;
import com.difotofoto.myeddb.model.Market;
import com.difotofoto.myeddb.model.StarSystem;
import com.difotofoto.myeddb.model.Station;
import com.difotofoto.myeddb.ui.MainFrame;

import java.awt.*;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class TradeRouteNode {
    public static enum LockOn {
        SYSTEM, STATION, COMMODITY
    }

    public static Color BEGIN_BACKGROUND = Constants.DARK_GREEN;
    public static Color BEGIN_FOREGROUND = Constants.LIGHT_GREEN;
    public static Color END_BACKGROUND = Constants.ED_DARKRED;
    public static Color END_FOREGROUND = Constants.ED_RED;
    public static Color BACKGROUND = Constants.ED_DARKRED;
    public static Color FOREGROUND = Constants.ED_ORANGE;

    private Dimension size = new Dimension(150, 100);
    private Point location = new Point(10, 10);

    private LockOn lockOn;
    private StarSystem starSystem;
    private Station station;
    private Commodity commodityToBuy;
    private Long buyPrice;
    private Integer supply;
    private Commodity commodityToSell;
    private Long sellPrice;
    private Integer demand;

    public TradeRouteNode(String sysName, String statName) {
        starSystem = new StarSystem();
        starSystem.setName(sysName);
        station = new Station();
        station.setName(statName);
        starSystem.getStations().add(station);
        station.setStarSystem(starSystem);
    }

    public TradeRouteNode(StarSystem starSystem) {
        lockOn = LockOn.SYSTEM;
        this.setStarSystem(starSystem);
    }

    public TradeRouteNode(Station station) {
        lockOn = LockOn.STATION;
        this.setStation(station);
        this.setStarSystem(station.getStarSystem());
    }

    public TradeRouteNode(Market market, boolean buy) {
        lockOn = LockOn.COMMODITY;
        this.setStation(market.getStation());
        this.setStarSystem(market.getStation().getStarSystem());
        if (buy) {
            this.setCommodityToBuy(market.getCommodity());
            this.setBuyPrice(market.getBuy());
            this.setSupply(market.getSupply());
        } else {
            this.setCommodityToSell(market.getCommodity());
            this.setSellPrice(market.getSell());
            this.setSupply(market.getDemand());
        }
    }

    public Rectangle getRectangle() {
        return new Rectangle(getLocation(), getSize());
    }

    public Dimension getSize() {
        return size;
    }

    public void setSize(Dimension size) {
        this.size = size;
    }

    public Point getLocation() {
        return location;
    }

    public void setLocation(Point location) {
        this.location = location;
    }

    public LockOn getLockOn() {
        return lockOn;
    }

    public void setLockOn(LockOn lockOn) {
        this.lockOn = lockOn;
    }

    public StarSystem getStarSystem() {
        return starSystem;
    }

    public void setStarSystem(StarSystem starSystem) {
        this.starSystem = starSystem;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Commodity getCommodityToBuy() {
        return commodityToBuy;
    }

    public void setCommodityToBuy(Commodity commodityToBuy) {
        this.commodityToBuy = commodityToBuy;
    }

    public Long getBuyPrice() {
        return buyPrice;
    }

    public void setBuyPrice(Long buyPrice) {
        this.buyPrice = buyPrice;
    }

    public Integer getSupply() {
        return supply;
    }

    public void setSupply(Integer supply) {
        this.supply = supply;
    }

    public Commodity getCommodityToSell() {
        return commodityToSell;
    }

    public void setCommodityToSell(Commodity commodityToSell) {
        this.commodityToSell = commodityToSell;
    }

    public Long getSellPrice() {
        return sellPrice;
    }

    public void setSellPrice(Long sellPrice) {
        this.sellPrice = sellPrice;
    }

    public Integer getDemand() {
        return demand;
    }

    public void setDemand(Integer demand) {
        this.demand = demand;
    }

    public static enum NodeType {
        Begin, End, Middle
    }

    public void paint(Graphics g, NodeType nodeType) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setFont(MainFrame.EUROCAPS_FONT.deriveFont(13f));

        Color bg = null;
        Color fg = null;
        if (nodeType.equals(NodeType.Begin)) {
            bg = BEGIN_BACKGROUND;
            fg = BEGIN_FOREGROUND;
        } else if (nodeType.equals(NodeType.End)) {
            bg = END_BACKGROUND;
            fg = END_FOREGROUND;
        } else {
            bg = BACKGROUND;
            fg = FOREGROUND;
        }

        String[] lines = new String[10];
        int lineCount = 0;
        if (this.getStation() == null) {
            lines[lineCount] = "T.B.D";
            lineCount++;
            lines[lineCount] = this.getStarSystem().getName();
            lineCount++;
        } else {
            lines[lineCount] = this.getStation().getName() + " (" + this.getStation().getDistanceToStar() + " ls)";
            lineCount++;
            lines[lineCount] = this.getStarSystem().getName();
            lineCount++;
        }

        lines[lineCount] = "";
        lineCount++;

        if (this.getCommodityToBuy() != null) {
            lines[lineCount] = "Buy " + getCommodityToBuy().getName();
            lineCount++;
            lines[lineCount] = "   For " + getBuyPrice();
            lineCount++;
        }
        if (this.getCommodityToSell() != null) {
            lines[lineCount] = "Sell " + getCommodityToSell().getName();
            lineCount++;
            lines[lineCount] = "   For " + getSellPrice();
            lineCount++;
        }

        int lineWidth = 0;
        FontMetrics mf = g2d.getFontMetrics();
        for (int l = 0; l < lines.length; l++) {
            if (lines[l] != null) {
                int actWidth = mf.stringWidth(lines[l]);
                if (actWidth > lineWidth) {
                    lineWidth = actWidth;
                }
            }
        }


        Dimension newDim = new Dimension(lineWidth + 10, (mf.getHeight() * lineCount) + 10);
        this.setSize(newDim);

        Graphics2D subg = (Graphics2D) g2d.create(location.x, location.y, size.width, size.height);
        subg.setRenderingHint(
                RenderingHints.KEY_TEXT_ANTIALIASING,
                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        subg.setPaint(bg);
        subg.fillRoundRect(0, 0, size.width - 1, size.height - 1, 14, 14);
        subg.setColor(fg);
        subg.drawRoundRect(0, 0, size.width - 1, size.height - 1, 14, 14);

        for (int i = 0; i < lineCount; i++) {
            if (lines[i].trim().length() > 0) {
                subg.drawString(lines[i], 5, mf.getHeight() * (i + 1));
            }
        }

        subg.drawLine(0, (mf.getHeight() * 3) - (mf.getHeight() / 2), size.width, (mf.getHeight() * 3) - (mf.getHeight() / 2));

//
//        Graphics2D subg = (Graphics2D) g2d.create(location.x, location.y, size.width, size.height);
//
//        subg.setRenderingHint(
//                RenderingHints.KEY_TEXT_ANTIALIASING,
//                RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
//
//        subg.setPaint(bg);
//        subg.fillRoundRect(0, 0, size.width - 1, size.height - 1, 14, 14);
//        subg.setColor(fg);
//        subg.drawRoundRect(0, 0, size.width - 1, size.height - 1, 14, 14);
//
//        Font line1Font = MainFrame.EUROCAPS_FONT.deriveFont(14f);
//        FontMetrics l1Metric = subg.getFontMetrics(line1Font);
//        subg.setFont(line1Font);
//        subg.drawString(this.getStarSystem().getName(), 4, l1Metric.getHeight());
//
//        int vOffset = l1Metric.getHeight();
//
//        Font line2Font = MainFrame.EUROCAPS_FONT.deriveFont(12f);
//        FontMetrics l2Metric = subg.getFontMetrics(line1Font);
//        subg.setFont(line2Font);
//        if (this.getStation() == null) {
//            subg.drawString("<< WAIT >>", 4, l1Metric.getHeight() + l2Metric.getHeight() - 6);
//        } else {
//            subg.drawString(this.getStation().getName(), 4, l1Metric.getHeight() + l2Metric.getHeight() - 6);
//        }
//        subg.drawLine(0, l1Metric.getHeight() + l2Metric.getHeight(), size.width, l1Metric.getHeight() + l2Metric.getHeight());
//
//        vOffset += l2Metric.getHeight();
//
//        if (this.getCommodityToBuy() != null) {
//            Font line3Font = MainFrame.EUROCAPS_FONT.deriveFont(12f);
//            FontMetrics l3Metric = subg.getFontMetrics(line3Font);
//            subg.setFont(line3Font);
//            subg.drawString("BUY " + getCommodityToBuy().getName(), 4, vOffset + l3Metric.getHeight());
//            vOffset += l3Metric.getHeight();
//            Font line4Font = MainFrame.EUROCAPS_FONT.deriveFont(12f);
//            FontMetrics l4Metric = subg.getFontMetrics(line4Font);
//            subg.setFont(line4Font);
//            subg.drawString("For " + getBuyPrice(), 14, vOffset + l4Metric.getHeight());
//            vOffset += l4Metric.getHeight();
//        }
//
//
//        if(this.getCommodityToSell() != null) {
//            Font line5Font = MainFrame.EUROCAPS_FONT.deriveFont(12f);
//            FontMetrics l5Metric = subg.getFontMetrics(line5Font);
//            subg.setFont(line5Font);
//                subg.drawString("SELL " + getCommodityToSell().getName(), 4, vOffset + l5Metric.getHeight());
//
//            vOffset += l5Metric.getHeight();
//
//            Font line6Font = MainFrame.EUROCAPS_FONT.deriveFont(12f);
//            FontMetrics l6Metric = subg.getFontMetrics(line6Font);
//            subg.setFont(line6Font);
//            subg.drawString("For " + getSellPrice(), 14, vOffset + l6Metric.getHeight() );
//        }
    }
}
