package com.difotofoto.myeddb.ui;

import java.awt.*;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public class FrameUtil {

    public static void center(Window window) {
        int screenHeight = Toolkit.getDefaultToolkit().getScreenSize().height;
        int screenWidth = Toolkit.getDefaultToolkit().getScreenSize().width;

        int windowHeight = window.getHeight();
        int windowWidth = window.getWidth();

        int x = (screenWidth / 2) - (windowWidth / 2);
        int y = (screenHeight / 2) - (windowHeight / 2);

        window.setLocation(new Point(x, y));
    }
}
