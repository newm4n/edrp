package com.difotofoto.myeddb.ui.model;

import com.difotofoto.myeddb.model.Station;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.*;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public class StationListModel implements ListModel<String> {
    private HashMap<String, Station> stationNameMap = new HashMap<>();
    private Vector<String> names = new Vector<>();
    private List<ListDataListener> listeners = new ArrayList<>();

    public void setStation(List<Station> stations) {
        int oldCount = names.size();
        stationNameMap.clear();
        names.clear();
        for (Station ss : stations) {
            stationNameMap.put(ss.getName(), ss);
        }
        names.addAll(stationNameMap.keySet());
        Collections.sort(names);
        ListDataEvent lde = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, Math.max(oldCount - 1, names.size() - 1));
        for (ListDataListener l : listeners) {
            l.contentsChanged(lde);
        }
    }

    public Station getStationWithName(String name) {
        return stationNameMap.get(name);
    }

    @Override
    public int getSize() {
        return names.size();
    }

    @Override
    public String getElementAt(int index) {
        return names.get(index);
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.remove(l);
    }
}
