package com.difotofoto.myeddb.ui.model;

import com.difotofoto.myeddb.model.Market;
import com.difotofoto.myeddb.model.Station;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ferdinand on 4/30/2016.
 */
public class StationTableModel extends AbstractTableModel {
    private String[] colName = new String[]{"Station Name", "Distance LS", "Type", "L Pad"};
    private List<Station> stations = new ArrayList<>();

    public void setStations(List<Station> stations) {
        int oldCount = this.stations == null ? 0 : this.stations.size();
        this.stations = stations;
        Collections.sort(this.stations);
        fireTableDataChanged();
    }

    public Station getStationAt(int index) {
        return stations.get(index);
    }

    @Override
    public int getRowCount() {
        return stations == null ? 0 : stations.size();
    }

    @Override
    public int getColumnCount() {
        return colName.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colName[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return Integer.class;
            case 2:
                return String.class;
            case 3:
                return String.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return stations.get(rowIndex).getName();
            case 1:
                return stations.get(rowIndex).getDistanceToStar();
            case 2:
                return stations.get(rowIndex).getType();
            case 3:
                return stations.get(rowIndex).getMaxLandingPadSize();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        // none are editable
    }
}
