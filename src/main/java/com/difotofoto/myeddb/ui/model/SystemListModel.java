package com.difotofoto.myeddb.ui.model;

import com.difotofoto.myeddb.model.StarSystem;

import javax.swing.*;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import java.util.*;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public class SystemListModel implements ListModel<String> {

    private HashMap<String, StarSystem> systemNameMap = new HashMap<>();
    private Vector<String> names = new Vector<>();
    private String search = "";

    private List<ListDataListener> listeners = new ArrayList<>();

    public void setStarSystems(List<StarSystem> systes) {
        systemNameMap.clear();
        for (StarSystem ss : systes) {
            systemNameMap.put(ss.getName(), ss);
        }
        setSearch(search);
    }

    public StarSystem getSystemWithName(String name) {
        return systemNameMap.get(name);
    }

    @Override
    public int getSize() {
        return names.size();
    }

    @Override
    public String getElementAt(int index) {
        return names.get(index);
    }

    public void setSearch(String search) {
        int oldCount = names.size();
        this.search = search;
        names.clear();
        String sup = search.toUpperCase();
        for (String s : systemNameMap.keySet()) {
            if (s.toUpperCase().contains(sup)) {
                names.add(s);
            }
        }
        Collections.sort(names);
        ListDataEvent lde = new ListDataEvent(this, ListDataEvent.CONTENTS_CHANGED, 0, Math.max(oldCount - 1, names.size() - 1));
        for (ListDataListener l : listeners) {
            l.contentsChanged(lde);
        }
    }

    @Override
    public void addListDataListener(ListDataListener l) {
        listeners.add(l);
    }

    @Override
    public void removeListDataListener(ListDataListener l) {
        listeners.remove(l);
    }
}
