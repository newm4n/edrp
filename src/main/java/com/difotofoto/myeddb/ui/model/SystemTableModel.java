package com.difotofoto.myeddb.ui.model;

import com.difotofoto.myeddb.model.StarSystem;
import com.difotofoto.myeddb.model.Station;

import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Ferdinand on 4/30/2016.
 */
public class SystemTableModel extends AbstractTableModel {
    private String[] colName = new String[]{"System Name", "Allegiance"};
    private List<StarSystem> systems = new ArrayList<>();
    private List<StarSystem> searchedSystem = new ArrayList<>();
    private String search = "";

    public StarSystem getSystemAt(int index) {
        return searchedSystem.get(index);
    }

    public void setStarSystems(List<StarSystem> systems) {
        this.systems = systems;
        setSearch(search);
    }

    public void setSearch(String search) {
        this.search = search;
        searchedSystem.clear();
        String sup = search.toUpperCase();
        for (StarSystem s : systems) {
            if (s.getName().toUpperCase().contains(sup)) {
                searchedSystem.add(s);
            }
        }
        Collections.sort(searchedSystem);
        this.fireTableDataChanged();
    }


    @Override
    public int getRowCount() {
        return searchedSystem == null ? 0 : searchedSystem.size();
    }

    @Override
    public int getColumnCount() {
        return colName.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colName[columnIndex];
    }


    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return String.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return searchedSystem.get(rowIndex).getName();
            case 1:
                return searchedSystem.get(rowIndex).getAllegiance();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        // none are editable
    }
}
