package com.difotofoto.myeddb.ui.model;

import com.difotofoto.myeddb.model.Market;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by ferdinand on 4/25/2016.
 */
public class MarketTableModel extends AbstractTableModel {


    private String[] colName = new String[]{"Item", "Buy", "Supply", "Sell", "Demand"};
    private List<Market> markets = new ArrayList<>();

    public void setMarkets(List<Market> markets) {
        int oldCount = this.markets == null ? 0 : this.markets.size();
        this.markets = markets;
        Collections.sort(this.markets);
        fireTableDataChanged();
    }

    public Market getMarketAt(int index) {
        return markets.get(index);
    }

    @Override
    public int getRowCount() {
        return markets == null ? 0 : markets.size();
    }

    @Override
    public int getColumnCount() {
        return colName.length;
    }

    @Override
    public String getColumnName(int columnIndex) {
        return colName[columnIndex];
    }

    @Override
    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return String.class;
            case 1:
                return Long.class;
            case 2:
                return Integer.class;
            case 3:
                return Long.class;
            case 4:
                return Integer.class;
            default:
                return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        switch (columnIndex) {
            case 0:
                return markets.get(rowIndex).getCommodity().getName();
            case 1:
                return markets.get(rowIndex).getBuy();
            case 2:
                return markets.get(rowIndex).getSupply();
            case 3:
                return markets.get(rowIndex).getSell();
            case 4:
                return markets.get(rowIndex).getDemand();
            default:
                return null;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        // none are editable
    }

}
