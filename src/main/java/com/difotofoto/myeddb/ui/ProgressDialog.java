package com.difotofoto.myeddb.ui;

import com.difotofoto.myeddb.importer.DownloadProgressListener;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ferdinand on 4/25/2016.
 */
public class ProgressDialog extends JDialog implements DownloadProgressListener {

    private JProgressBar progress = new JProgressBar();
    private JLabel lbSaving = new JLabel();

    public void setMinimum(int min) {
        progress.setMinimum(min);
    }

    public void setMax(int max) {
        progress.setMaximum(max);
    }

    public void setValue(int value) {
        progress.setValue(value);
    }

    public ProgressDialog(Frame owner, String title) {

        super(owner, title, true);

        progress.setPreferredSize(new Dimension(400, 20));
        lbSaving.setPreferredSize(new Dimension(400, 20));

        this.setLayout(new GridBagLayout());
        JPanel panel = new JPanel();
        panel.setLayout(new GridBagLayout());

        GridBagConstraints constraints = new GridBagConstraints();

        constraints.gridx = 0;
        constraints.gridy = 0;
        panel.add(progress, constraints);
        constraints.gridy = 1;
        panel.add(lbSaving, constraints);

        panel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        this.setContentPane(panel);
        this.pack();
        FrameUtil.center(this);
    }

    @Override
    public void contentLength(long length) {
        setMinimum(0);
        setMax((int) length);
    }

    @Override
    public void start() {
//        Thread t = new Thread() {
//            public void run() {
//                ProgressDialog.this.setVisible(true);
//            }
//        };
//        t.start();
    }

    @Override
    public void finish() {
        if (this.isVisible())
            this.setVisible(false);
    }

    @Override
    public void downloaded(long downloaded) {
        lbSaving.setText("System " + downloaded + " of " + progress.getMaximum());
        setValue((int) downloaded);
    }
}
