package com.difotofoto.myeddb.ui;

import com.difotofoto.myeddb.Graph;
import com.difotofoto.myeddb.model.Market;
import com.difotofoto.myeddb.model.StarSystem;
import com.difotofoto.myeddb.model.Station;
import com.difotofoto.myeddb.templating.VelocityTemplate;
import com.difotofoto.myeddb.ui.editor.TradeEditorComponent;
import com.difotofoto.myeddb.ui.editor.TradeRouteNode;
import com.difotofoto.myeddb.ui.model.MarketTableModel;
import com.difotofoto.myeddb.ui.model.StationTableModel;
import com.difotofoto.myeddb.ui.model.SystemListModel;
import com.difotofoto.myeddb.ui.model.SystemTableModel;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Ferdinand on 4/24/2016.
 */
public class MainFrame extends JFrame {

    private static Logger log = Logger.getLogger(MainFrame.class);

    private Graph graph = null;

    private File saveToFile = null;

    // MENU
    JMenuBar menuBar = new JMenuBar();
    JMenu fileMenu = new JMenu("File");
    JMenuItem exitGraphMenu = new JMenuItem("Exit");

    JMenu systemMenu = new JMenu("Star System");
    JMenuItem loadFromWebMenu = new JMenuItem("Load From Web");
    JMenuItem openGraphMenu = new JMenuItem("Open");
    JMenuItem saveGraphMenu = new JMenuItem("Save");
    JMenuItem saveAsGraphMenu = new JMenuItem("Save As");

    JMenu analysisMenu = new JMenu("Analysis");
    JMenuItem analysisFilters = new JMenuItem("Filter Star Systems");
    JMenuItem analysisShortHop = new JMenuItem("Most Profitable Short Loop");

    JMenu reportMenu = new JMenu("Report");
    JMenuItem saveHtmlReport = new JMenuItem("Make HTML Report");
    JMenuItem saveTxtReport = new JMenuItem("Make TXT Report");
    JMenuItem saveJsonReport = new JMenuItem("Make JSON Report");

    JMenu helpMenu = new JMenu("Help");
    JMenuItem instruction = new JMenuItem("Usage");
    JMenuItem about = new JMenuItem("About");

    JTextField dfSearchField = new JTextField();

    JTable tblSystem = new JTable();
    JScrollPane systemScroll = new JScrollPane(tblSystem);
    SystemTableModel systemTableModel = new SystemTableModel();

    JTable tblStation = new JTable();
    JScrollPane stationListScroll = new JScrollPane(tblStation);
    StationTableModel stationTableModel = new StationTableModel();

    JTable tblMarket = new JTable();
    JScrollPane marketTableScroll = new JScrollPane(tblMarket);
    MarketTableModel marketTableModel = new MarketTableModel();

    TradeEditorComponent editor = new TradeEditorComponent();

    JButton pbAddSystem = new JButton("Add System");
    JButton pbAddStation = new JButton("Add Station");
    JButton pbAddCommodity = new JButton("Add Commodity");

    private void graphStateLoaded(boolean loaded) {
        saveGraphMenu.setEnabled(loaded);
        saveAsGraphMenu.setEnabled(loaded);
    }


    public MainFrame() {

        this.setTitle("EDRP");
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);


        menuBar.add(fileMenu);
        fileMenu.add(exitGraphMenu);

        menuBar.add(systemMenu);
        systemMenu.add(loadFromWebMenu);
        systemMenu.add(openGraphMenu);
        systemMenu.add(new JSeparator());
        systemMenu.add(saveGraphMenu);
        systemMenu.add(saveAsGraphMenu);

        menuBar.add(analysisMenu);
        analysisMenu.add(analysisFilters);
        analysisMenu.add(analysisShortHop);

        menuBar.add(reportMenu);
        reportMenu.add(saveHtmlReport);
        reportMenu.add(saveTxtReport);
        reportMenu.add(saveJsonReport);

        menuBar.add(helpMenu);
        helpMenu.add(instruction);
        helpMenu.add(about);


        graphStateLoaded(false);
        this.setJMenuBar(menuBar);

        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                onClosing();
            }
        });

        dfSearchField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                systemTableModel.setSearch(dfSearchField.getText().trim());
                tblSystem.clearSelection();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                systemTableModel.setSearch(dfSearchField.getText().trim());
                tblSystem.clearSelection();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {

            }
        });

        openGraphMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOpen();
            }
        });
        loadFromWebMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onOpenFromWeb();
            }
        });
        saveGraphMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSave();
            }
        });
        saveAsGraphMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onSaveAs();
            }
        });
        exitGraphMenu.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onExit();
            }
        });

        tblSystem.setModel(systemTableModel);
        tblStation.setModel(stationTableModel);
        tblMarket.setModel(marketTableModel);
        tblSystem.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblStation.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblMarket.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        tblMarket.setForeground(Color.BLACK);

        pbAddCommodity.setPreferredSize(new Dimension(200, 24));
        pbAddSystem.setPreferredSize(new Dimension(200, 24));
        pbAddStation.setPreferredSize(new Dimension(200, 24));

        pbAddCommodity.setEnabled(false);
        pbAddSystem.setEnabled(false);
        pbAddStation.setEnabled(false);

        tblSystem.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                StarSystem system = null;
                if(tblSystem.getSelectedRow() >= 0) {
                    system = systemTableModel.getSystemAt(tblSystem.getSelectedRow());
                }
                if (system == null) {
                    stationTableModel.setStations(new ArrayList<Station>());
                } else {
                    stationTableModel.setStations(system.getStations());
                }
                tblStation.clearSelection();
                pbAddStation.setEnabled(false);
                if (tblSystem.getSelectedRow() >= 0) {
                    pbAddSystem.setEnabled(true);
                } else {
                    pbAddSystem.setEnabled(false);
                }
            }
        });

        tblStation.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {

                Station station = null;
                if(tblStation.getSelectedRow() >= 0) {
                    station = stationTableModel.getStationAt(tblStation.getSelectedRow());
                }
                if (station == null)
                {
                    marketTableModel.setMarkets(new ArrayList<Market>());
                } else {
                    marketTableModel.setMarkets(station.getMarkets());
                    pbAddStation.setEnabled(true);
                }

                SwingUtilities.invokeLater(new Runnable() {
                       @Override
                       public void run() {
                           marketTableScroll.invalidate();
                           tblMarket.validate();
                       }
                   }
                );

                if (tblStation.getSelectedRow() >= 0)
                {
                    pbAddStation.setEnabled(true);
                } else {
                    pbAddStation.setEnabled(false);
                }
            }
        });

        tblMarket.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                if (tblMarket.getSelectedRow() >= 0) {
                    pbAddCommodity.setEnabled(true);
                } else {
                    pbAddCommodity.setEnabled(false);
                }
            }
        });

        JPanel systemListPanel = new JPanel(new BorderLayout());
        systemListPanel.add(dfSearchField, BorderLayout.NORTH);
        systemListPanel.add(systemScroll, BorderLayout.CENTER);
        systemListPanel.add(pbAddSystem, BorderLayout.SOUTH);
        systemListPanel.setPreferredSize(new Dimension(300, 200));


        JPanel stationListPanel = new JPanel(new BorderLayout());
        stationListPanel.add(stationListScroll, BorderLayout.CENTER);
        stationListPanel.add(pbAddStation, BorderLayout.SOUTH);

        stationListPanel.setPreferredSize(new Dimension(400, 200));

        JPanel innerPanel = new JPanel(new BorderLayout());
        innerPanel.add(stationListPanel, BorderLayout.WEST);
        JPanel rightPanel = new JPanel(new BorderLayout());
        rightPanel.add(marketTableScroll, BorderLayout.CENTER);
        rightPanel.add(pbAddCommodity, BorderLayout.SOUTH);
        innerPanel.add(rightPanel, BorderLayout.CENTER);

        pbAddCommodity.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddCommodity();
            }
        });

        pbAddStation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddStation();
            }
        });

        pbAddSystem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                onAddSystem();
            }
        });


        saveHtmlReport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(editor.getTradeRoutes().size() == 0) {
                    JOptionPane.showMessageDialog(MainFrame.this, "There are no routes in the diagram." , "Create Report", JOptionPane.ERROR_MESSAGE);
                    return;
                }
                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "HyperText Markup Language", "html");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showSaveDialog(MainFrame.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File choosen = chooser.getSelectedFile();
                    if(!choosen.getName().toUpperCase().endsWith(".HTML")) {
                        choosen = new File(choosen.getAbsolutePath() + ".html");
                    }
                    if(choosen.exists()) {
                        int ans = JOptionPane.showConfirmDialog(MainFrame.this, "File " + choosen.getAbsolutePath() + " is exist. Overwrite ?", "Save Report", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                        if(ans == JOptionPane.YES_OPTION) {
                            choosen.delete();
                        } else {
                            return;
                        }
                    }
                    try {
                        choosen.createNewFile();
                        FileOutputStream fos = new FileOutputStream(choosen);
                        fos.write(VelocityTemplate.htmlTemplate.processTemplate(editor.getTradeRoutes()).getBytes());
                        fos.flush();
                        fos.close();
                    }
                    catch(IOException ioe) {
                        JOptionPane.showMessageDialog(MainFrame.this, "Error while saving report into " + choosen.getAbsolutePath() + " ; " + ioe.getMessage(), "Save Report", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        saveJsonReport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(editor.getTradeRoutes().size() == 0) {
                    JOptionPane.showMessageDialog(MainFrame.this, "There are no routes in the diagram." , "Create Report", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "JavaScript Object Notation", "json");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showSaveDialog(MainFrame.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File choosen = chooser.getSelectedFile();
                    if(!choosen.getName().toUpperCase().endsWith(".JSON")) {
                        choosen = new File(choosen.getAbsolutePath() + ".json");
                    }
                    if(choosen.exists()) {
                        int ans = JOptionPane.showConfirmDialog(MainFrame.this, "File " + choosen.getAbsolutePath() + " is exist. Overwrite ?", "Save Report", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                        if(ans == JOptionPane.YES_OPTION) {
                            choosen.delete();
                        } else {
                            return;
                        }
                    }
                    try {
                        choosen.createNewFile();
                        FileOutputStream fos = new FileOutputStream(choosen);
                        fos.write(VelocityTemplate.jsonTemplate.processTemplate(editor.getTradeRoutes()).getBytes());
                        fos.flush();
                        fos.close();
                    }
                    catch(IOException ioe) {
                        JOptionPane.showMessageDialog(MainFrame.this, "Error while saving report into " + choosen.getAbsolutePath() + " ; " + ioe.getMessage(), "Save Report", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        saveTxtReport.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(editor.getTradeRoutes().size() == 0) {
                    JOptionPane.showMessageDialog(MainFrame.this, "There are no routes in the diagram." , "Create Report", JOptionPane.ERROR_MESSAGE);
                    return;
                }

                JFileChooser chooser = new JFileChooser();
                FileNameExtensionFilter filter = new FileNameExtensionFilter(
                        "Plain Text", "txt");
                chooser.setFileFilter(filter);
                int returnVal = chooser.showSaveDialog(MainFrame.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    File choosen = chooser.getSelectedFile();
                    if(!choosen.getName().toUpperCase().endsWith(".TXT")) {
                        choosen = new File(choosen.getAbsolutePath() + ".txt");
                    }
                    if(choosen.exists()) {
                        int ans = JOptionPane.showConfirmDialog(MainFrame.this, "File " + choosen.getAbsolutePath() + " is exist. Overwrite ?", "Save Report", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                        if(ans == JOptionPane.YES_OPTION) {
                            choosen.delete();
                        } else {
                            return;
                        }
                    }
                    try {
                        choosen.createNewFile();
                        FileOutputStream fos = new FileOutputStream(choosen);
                        fos.write(VelocityTemplate.txtTemplate.processTemplate(editor.getTradeRoutes()).getBytes());
                        fos.flush();
                        fos.close();
                    }
                    catch(IOException ioe) {
                        JOptionPane.showMessageDialog(MainFrame.this, "Error while saving report into " + choosen.getAbsolutePath() + " ; " + ioe.getMessage(), "Save Report", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

        JPanel mainPanel = new JPanel(new BorderLayout());
        JPanel topPanel = new JPanel(new BorderLayout());
        this.setContentPane(mainPanel);
        topPanel.add(systemListPanel, BorderLayout.WEST);
        topPanel.add(innerPanel, BorderLayout.CENTER);
        topPanel.setPreferredSize(new Dimension(200, 400));
        mainPanel.add(topPanel, BorderLayout.NORTH);
        mainPanel.add(editor, BorderLayout.CENTER);

        // Need to place the routing UI in the center of main panel

        // pack();
        this.setPreferredSize(new Dimension(1100, 800));
        this.setSize(getPreferredSize());
        FrameUtil.center(this);
    }

    public void onAddCommodity() {
        if(tblMarket.getSelectedRow() < 0) return;
        Market market = marketTableModel.getMarketAt(tblMarket.getSelectedRow());
        TradeRouteNode trn = new TradeRouteNode(market, true);
        editor.addTradeRouteNode(trn);
        editor.repaint();
    }

    public void onAddStation() {
        if(tblStation.getSelectedRow() < 0) return;
        Station station = stationTableModel.getStationAt(tblStation.getSelectedRow());
        TradeRouteNode trn = new TradeRouteNode(station);
        editor.addTradeRouteNode(trn);
        editor.repaint();
    }

    public void onAddSystem() {
        if(tblSystem.getSelectedRow() < 0) return;
        StarSystem system = systemTableModel.getSystemAt(tblSystem.getSelectedRow());
        TradeRouteNode trn = new TradeRouteNode(system);
        editor.addTradeRouteNode(trn);
        editor.repaint();
    }

    public void onOpen() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Graph File", "edg");
        chooser.setFileFilter(filter);
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File selectedFile = chooser.getSelectedFile();
            graph = loadFromFile(selectedFile);
            List<StarSystem> systems = new ArrayList<>();
            systems.addAll(graph.getSystems().values());
            systemTableModel.setStarSystems(systems);
            graphStateLoaded(true);
            setTitle(graph.getGraphDate());
        }
    }

    public void onOpenFromWeb() {
        DownloadDialog download = new DownloadDialog();
        download.setVisible(true);
        if (download.success) {
            graph = download.getGraph();
            List<StarSystem> systems = new ArrayList<>();
            systems.addAll(graph.getSystems().values());
            systemTableModel.setStarSystems(systems);
            graphStateLoaded(true);
            setTitle(graph.getGraphDate());
        }
    }

    public void setTitle(Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd MMMM yyyy - hh.mm.ss");
        this.setTitle("EDRP - File Date : " + sdf.format(date));
    }

    public void onSave() {
        if (saveToFile != null) {
            try {
                if (saveToFile.exists()) saveToFile.delete();
                saveToFile.createNewFile();
                saveToFile(graph, saveToFile);
            } catch (IOException ioe) {
                JOptionPane.showMessageDialog(this, "Error while saving graph into " + saveToFile.getAbsolutePath() + ". " + ioe.getMessage(), "Save Error", JOptionPane.ERROR_MESSAGE);

            }
        } else {
            onSaveAs();
        }
    }

    public void onSaveAs() {
        JFileChooser chooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Graph File", "edg");
        chooser.setFileFilter(filter);
        boolean done = false;
        while (done == false) {
            int returnVal = chooser.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File selected = chooser.getSelectedFile();
                if (selected.exists()) {
                    if (JOptionPane.showConfirmDialog(this, "File " + selected.getAbsolutePath() + " is exist. Overwrite ?", "Save As", JOptionPane.YES_NO_OPTION) == JOptionPane.YES_OPTION) {
                        try {
                            selected.delete();
                            selected.createNewFile();
                            saveToFile(graph, selected);
                            saveToFile = selected;
                        } catch (IOException ioe) {
                            JOptionPane.showMessageDialog(this, "Error while saving graph into " + saveToFile.getAbsolutePath() + ". " + ioe.getMessage(), "Save Error", JOptionPane.ERROR_MESSAGE);
                        }
                        done = true;
                    }
                } else {
                    try {
                        if (!selected.getName().toUpperCase().endsWith(".EDG")) {
                            selected = new File(selected.getAbsolutePath() + ".edg");
                        }
                        selected.createNewFile();
                        saveToFile(graph, selected);
                        saveToFile = selected;
                    } catch (IOException ioe) {
                        JOptionPane.showMessageDialog(this, "Error while saving graph into " + saveToFile.getAbsolutePath() + ". " + ioe.getMessage(), "Save Error", JOptionPane.ERROR_MESSAGE);
                    }
                    done = true;
                }
            } else if (returnVal == JFileChooser.CANCEL_OPTION) {
                done = true;
            }
        }
    }

    public void onExit() {
        if (graph != null && saveToFile == null) {
            int option = JOptionPane.showConfirmDialog(this, "Graph has not been saved, are you sure to exit without saving ?", "Exit", JOptionPane.YES_NO_CANCEL_OPTION);
            if (option == JOptionPane.YES_OPTION) {
                System.exit(0);
            } else if (option == JOptionPane.NO_OPTION) {
                onSaveAs();
                System.exit(0);
            } else {
                // ignore exit.
            }
        } else {
            System.exit(0);
        }
    }

    public void onClosing() {
    }

    public static Font EUROCAPS_FONT = null;

    static {
        try {
            InputStream is = MainFrame.class.getResourceAsStream("/EUROCAPS.TTF");
            EUROCAPS_FONT = Font.createFont(Font.TRUETYPE_FONT, is);
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            ge.registerFont(EUROCAPS_FONT);

//            SynthLookAndFeel laf = new SynthLookAndFeel();
//            UIManager.setLookAndFeel(laf);
//            SynthLookAndFeel.setStyleFactory(new EliteDangerousStyleFactory());

        } catch (IOException ioe) {
            log.error("IO Exception while loading font.", ioe);
        } catch (FontFormatException ffe) {
            log.error("Invalid Font Format.", ffe);
        }
//        catch(UnsupportedLookAndFeelException ulefe) {
//            log.error("Unsupported Look and Feel", ulefe);
//        }
    }

    public void saveToFile(Graph graph, File fileToSave) {
        ProgressDialog progressDialog = new ProgressDialog(this, "Saving");
        progressDialog.addWindowListener(new WindowAdapter() {
            Thread workerThread = new Thread() {
                @Override
                public void run() {
                    graph.setProgressListener(progressDialog);
                    try {
                        graph.save(fileToSave);
                    } catch (Exception e) {
                    } finally {
                        progressDialog.finish();
                    }
                }
            };

            @Override
            public void windowOpened(WindowEvent e) {
                workerThread.start();
            }
        });
        progressDialog.setVisible(true);
    }

    public Graph loadFromFile(File fileToLoad) {
        final Graph g = new Graph();
        ProgressDialog progressDialog = new ProgressDialog(this, "Loading");
        progressDialog.addWindowListener(new WindowAdapter() {
            Thread workerThread = new Thread() {
                @Override
                public void run() {
                    g.setProgressListener(progressDialog);
                    try {
                        g.load(fileToLoad);
                    } catch (Exception e) {
                        log.error("Exception thrown while loading : " + e.getMessage(), e);
                        JOptionPane.showMessageDialog(MainFrame.this, "Error while loading : " + e.getMessage(), "Loading", JOptionPane.ERROR_MESSAGE);
                    } finally {
                        progressDialog.finish();
                    }
                }
            };

            @Override
            public void windowOpened(WindowEvent e) {
                workerThread.start();
            }
        });
        progressDialog.setVisible(true);
        return g;
    }


}
