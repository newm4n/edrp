package com.difotofoto.myeddb.ui.lookfeel.styles;

import com.difotofoto.myeddb.ui.MainFrame;
import com.difotofoto.myeddb.ui.lookfeel.EliteDangerousPainter;
import org.apache.log4j.Logger;

import javax.swing.plaf.synth.*;
import java.awt.*;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public abstract class EliteDangerousSynthStyle extends SynthStyle {

    Logger log = Logger.getLogger(EliteDangerousSynthStyle.class);

    public abstract Color getColorForState(SynthContext context, boolean enable, boolean disable, boolean pressed, boolean mouseOver, boolean selected, boolean focused, boolean devault, ColorType type);

    @Override
    protected Color getColorForState(SynthContext context, ColorType type) {
        boolean enable = (context.getComponentState() | SynthConstants.ENABLED) == SynthConstants.ENABLED;
        boolean disabled = (context.getComponentState() | SynthConstants.DISABLED) == SynthConstants.DISABLED;
        boolean pressed = (context.getComponentState() | SynthConstants.PRESSED) == SynthConstants.PRESSED;
        boolean mouseOver = (context.getComponentState() | SynthConstants.MOUSE_OVER) == SynthConstants.MOUSE_OVER;
        boolean selected = (context.getComponentState() | SynthConstants.SELECTED) == SynthConstants.SELECTED;
        boolean focused = (context.getComponentState() | SynthConstants.FOCUSED) == SynthConstants.FOCUSED;
        boolean devault = (context.getComponentState() | SynthConstants.DEFAULT) == SynthConstants.DEFAULT;
        return getColorForState(context, enable, disabled, pressed, mouseOver, selected, focused, devault, type);
    }

    @Override
    protected Font getFontForState(SynthContext context) {
        return MainFrame.EUROCAPS_FONT.deriveFont(14.0f);
    }

    @Override
    public SynthPainter getPainter(SynthContext context) {
        return new EliteDangerousPainter();
    }
}
