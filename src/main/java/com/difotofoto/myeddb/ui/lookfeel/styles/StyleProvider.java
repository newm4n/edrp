package com.difotofoto.myeddb.ui.lookfeel.styles;

import javax.swing.*;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthStyle;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public interface StyleProvider {

    public SynthStyle styleForRegion(JComponent component, Region id);

}
