package com.difotofoto.myeddb.ui.lookfeel;

import com.difotofoto.myeddb.ui.lookfeel.styles.*;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthStyle;
import javax.swing.plaf.synth.SynthStyleFactory;

/**
 * Created by Ferdinand on 4/26/2016.
 */
public class EliteDangerousStyleFactory extends SynthStyleFactory {
    Logger log = Logger.getLogger(EliteDangerousStyleFactory.class);
    private EliteDangerousDefaultStyle defaultStyleProvider = new EliteDangerousDefaultStyle();
    private EliteDangerousListStyle listStyleProvider = new EliteDangerousListStyle();
    private EliteDangerousLabelStyle labelStyleProvider = new EliteDangerousLabelStyle();
    private EliteDangerousButtonStyle buttonStyleProvider = new EliteDangerousButtonStyle();
    private EliteDangerousArrowButtonStyle arrowButtonStyleProvider = new EliteDangerousArrowButtonStyle();
    private EliteDangerousTreeStyle treeStyleProvider = new EliteDangerousTreeStyle();
    private EliteDangerousMenuBarStyle menuBarStyleProvider = new EliteDangerousMenuBarStyle();
    private EliteDangerousMenuItemStyle menuItemStyleProvider = new EliteDangerousMenuItemStyle();
    private EliteDangerousPopupMenuStyle popupMenuStyleProvider = new EliteDangerousPopupMenuStyle();
    private EliteDangerousProgressBarStyle progressBarStyleProvider = new EliteDangerousProgressBarStyle();
    private EliteDangerousMenuStyle menuStyleProvider = new EliteDangerousMenuStyle();
    private EliteDangerousScrollPaneStyle scrollPaneStyleProvider = new EliteDangerousScrollPaneStyle();
    private EliteDangerousScrollBarStyle scrollBarStyleProvider = new EliteDangerousScrollBarStyle();
    private EliteDangerousPanelStyle panelStyleProvider = new EliteDangerousPanelStyle();
    private EliteDangerousRootPaneStyle rootPaneStyleProvider = new EliteDangerousRootPaneStyle();
    private EliteDangerousTextFieldStyle textFieldStyleProvider = new EliteDangerousTextFieldStyle();
    private EliteDangerousTableHeaderStyle tableHeaderStyleProvider = new EliteDangerousTableHeaderStyle();
    private EliteDangerousTableStyle tableStyleProvider = new EliteDangerousTableStyle();
    private EliteDangerousViewportStyle viewportStyleProvider = new EliteDangerousViewportStyle();
    private EliteDangerousFileChooserStyle fileChooserStyleProvider = new EliteDangerousFileChooserStyle();
    private EliteDangerousToolTipStyle toolTipStyleProvider = new EliteDangerousToolTipStyle();

    @Override
    public SynthStyle getStyle(JComponent c, Region id) {
        if (c.getClass().getName().equals("javax.swing.JList") ||
                c.getClass().getName().equals("javax.swing.plaf.synth.SynthListUI$SynthListCellRenderer")) {
            return listStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().equals("javax.swing.JLabel")) {
            return labelStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().equals("javax.swing.JButton")) {
            return buttonStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().equals(JTree.class)) {
            return treeStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().equals(JMenuBar.class)) {
            return menuBarStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().equals(JMenuItem.class)) {
            return menuItemStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().equals(JPopupMenu.class) ||
                c.getClass().getName().equals("javax.swing.plaf.synth.SynthComboPopup")) {
            return popupMenuStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().equals(JProgressBar.class)) {
            return progressBarStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().equals(JMenu.class)) {
            return menuStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().equals(JScrollPane.class)) {
            return scrollPaneStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().equals("javax.swing.JRootPane")) {
            return rootPaneStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().equals("javax.swing.JToolTip")) {
            return toolTipStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().equals("javax.swing.JPanel")) {
            return panelStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().equals("javax.swing.JScrollPane$ScrollBar")) {
            return scrollBarStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().startsWith("javax.swing.plaf.synth.SynthScrollBarUI") ||
                c.getClass().getName().equals("javax.swing.plaf.synth.SynthArrowButton")) {
            return arrowButtonStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().startsWith("javax.swing.JTextField")) {
            return textFieldStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().startsWith("javax.swing.JFileChooser")) {
            return fileChooserStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().startsWith("javax.swing.table.JTableHeader") ||
                c.getClass().getName().equals("sun.swing.table.DefaultTableCellHeaderRenderer") ||
                c.getClass().getName().equals("javax.swing.plaf.synth.SynthTableHeaderUI$HeaderRenderer")) {
            return tableHeaderStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().startsWith("javax.swing.JTable") ||
                c.getClass().getName().equals("javax.swing.plaf.synth.SynthTableUI$SynthBooleanTableCellRenderer") ||
                c.getClass().getName().equals("javax.swing.plaf.synth.SynthTableUI$SynthTableCellRenderer") ||
                c.getClass().getName().equals("javax.swing.table.DefaultTableCellRenderer$UIResource")) {
            return tableStyleProvider.styleForRegion(c, id);
        } else if (c.getClass().getName().startsWith("javax.swing.JViewport")) {
            return viewportStyleProvider.styleForRegion(c, id);
        } else {
            log.warn("Unhandled Swing Class : " + c.getClass().getName());
            return defaultStyleProvider.styleForRegion(c, id);
        }
    }
}
