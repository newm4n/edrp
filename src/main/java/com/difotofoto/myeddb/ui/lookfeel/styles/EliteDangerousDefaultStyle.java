package com.difotofoto.myeddb.ui.lookfeel.styles;

import com.difotofoto.myeddb.ui.MainFrame;
import com.difotofoto.myeddb.ui.lookfeel.EliteDangerousPainter;
import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.plaf.synth.*;
import java.awt.*;

/**
 * Created by Ferdinand on 4/26/2016.
 */
public class EliteDangerousDefaultStyle implements StyleProvider {
    Logger log = Logger.getLogger(EliteDangerousDefaultStyle.class);

    @Override
    public SynthStyle styleForRegion(JComponent component, Region id) {
        return getDefaultStyle(component, id);
    }

    protected SynthStyle getDefaultStyle(JComponent component, Region id) {
        log.warn("Unhandled Region : " + id.getName() + " in class " + component.getClass().getName());
        return new SynthStyle() {
            @Override
            protected Color getColorForState(SynthContext context, ColorType type) {
                if (type.equals(ColorType.FOREGROUND)) {
                    return ED_ORANGE;
                } else if (type.equals(ColorType.BACKGROUND)) {
                    return Color.BLACK;
                } else if (type.equals(ColorType.TEXT_FOREGROUND)) {
                    return ED_ORANGE;
                } else if (type.equals(ColorType.TEXT_BACKGROUND)) {
                    return Color.BLACK;
                } else if (type.equals(ColorType.FOCUS)) {
                    return ED_YELLOW;
                }
                return null;
            }

            @Override
            protected Font getFontForState(SynthContext context) {
                return MainFrame.EUROCAPS_FONT.deriveFont(14.0f);
            }

            @Override
            public Insets getInsets(SynthContext context, Insets insets) {
                return new Insets(0, 0, 0, 0);
            }

            @Override
            public SynthPainter getPainter(SynthContext context) {
                return new EliteDangerousPainter();
            }
        };
    }

    public static Color ED_ORANGE = new Color(175, 82, 10);
    public static Color ED_RED = new Color(191, 10, 10);
    public static Color ED_YELLOW = new Color(181, 127, 16);
    public static Color ED_DARKRED = new Color(66, 20, 10);

}
