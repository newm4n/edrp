package com.difotofoto.myeddb.ui.lookfeel.styles;

import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.plaf.synth.ColorType;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthStyle;
import java.awt.*;

/**
 * Created by Ferdinand on 4/26/2016.
 */
public class EliteDangerousMenuItemStyle extends EliteDangerousDefaultStyle {
    Logger log = Logger.getLogger(EliteDangerousMenuItemStyle.class);

    @Override
    public SynthStyle styleForRegion(JComponent component, Region id) {
        if (id.equals(Region.MENU_ITEM)) {
            return new EliteDangerousSynthStyle() {
                @Override
                public Color getColorForState(SynthContext context, boolean enable, boolean disable, boolean pressed, boolean mouseOver, boolean selected, boolean focused, boolean devault, ColorType type) {
                    if (disable) {
                        if (type.equals(ColorType.FOREGROUND)) return Color.DARK_GRAY;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLACK;
                        if (type.equals(ColorType.TEXT_FOREGROUND)) return Color.DARK_GRAY;
                        if (type.equals(ColorType.TEXT_BACKGROUND)) return Color.BLACK;
                    } else if (mouseOver) {
                        if (type.equals(ColorType.FOREGROUND)) return ED_ORANGE;
                        if (type.equals(ColorType.BACKGROUND)) return ED_DARKRED;
                        if (type.equals(ColorType.TEXT_FOREGROUND)) return ED_ORANGE;
                        if (type.equals(ColorType.TEXT_BACKGROUND)) return ED_DARKRED;
                    } else if (enable) {
                        if (type.equals(ColorType.FOREGROUND)) return ED_ORANGE;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLACK;
                        if (type.equals(ColorType.TEXT_FOREGROUND)) return ED_ORANGE;
                        if (type.equals(ColorType.TEXT_BACKGROUND)) return Color.BLACK;
                    }
                    log.warn("UNHANDLED : REG:" + id + " EN:" + (enable ? "T" : "F") + " DIS:" + (disable ? "T" : "F") + " PRS:" + (pressed ? "T" : "F") + " MOV:" + (mouseOver ? "T" : "F") + " SEL:" + (selected ? "T" : "F") + " FOC:" + (focused ? "T" : "F") + " DEV:" + (devault ? "T" : "F") + " - " + type);
                    return Color.WHITE;
                }

                @Override
                public Insets getInsets(SynthContext context, Insets insets) {
                    return new Insets(3, 5, 3, 5);
                }
            };
        } else if (id.equals(Region.MENU_ITEM_ACCELERATOR)) {
            return new EliteDangerousSynthStyle() {
                @Override
                public Color getColorForState(SynthContext context, boolean enable, boolean disable, boolean pressed, boolean mouseOver, boolean selected, boolean focused, boolean devault, ColorType type) {
                    if (enable) {
                        if (type.equals(ColorType.FOREGROUND)) return ED_ORANGE;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLACK;
                    }
                    log.warn("UNHANDLED : REG:" + id + " EN:" + (enable ? "T" : "F") + " DIS:" + (disable ? "T" : "F") + " PRS:" + (pressed ? "T" : "F") + " MOV:" + (mouseOver ? "T" : "F") + " SEL:" + (selected ? "T" : "F") + " FOC:" + (focused ? "T" : "F") + " DEV:" + (devault ? "T" : "F") + " - " + type);
                    return Color.WHITE;
                }

                @Override
                public Insets getInsets(SynthContext context, Insets insets) {
                    return new Insets(3, 5, 3, 5);
                }
            };
        } else {
            return super.getDefaultStyle(component, id);
        }
    }


}
