package com.difotofoto.myeddb.ui.lookfeel.styles;

import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.plaf.synth.ColorType;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthStyle;
import java.awt.*;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class EliteDangerousScrollPaneStyle extends EliteDangerousDefaultStyle {
    Logger log = Logger.getLogger(EliteDangerousScrollPaneStyle.class);

    @Override
    public SynthStyle styleForRegion(JComponent component, Region id) {
        if (id.equals(Region.SCROLL_PANE)) {
            return new EliteDangerousSynthStyle() {
                @Override
                public Color getColorForState(SynthContext context, boolean enable, boolean disable, boolean pressed, boolean mouseOver, boolean selected, boolean focused, boolean devault, ColorType type) {
                    if (selected) {
                        if (type.equals(ColorType.FOREGROUND)) return Color.BLACK;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLUE;
                        if (type.equals(ColorType.TEXT_BACKGROUND)) return Color.BLUE;
                    } else if (enable) {
                        if (type.equals(ColorType.FOREGROUND)) return Color.ORANGE;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLACK;
                    }
                    log.warn("UNHANDLED : REG:" + id + " EN:" + (enable ? "T" : "F") + " DIS:" + (disable ? "T" : "F") + " PRS:" + (pressed ? "T" : "F") + " MOV:" + (mouseOver ? "T" : "F") + " SEL:" + (selected ? "T" : "F") + " FOC:" + (focused ? "T" : "F") + " DEV:" + (devault ? "T" : "F") + " - " + type);
                    return Color.WHITE;
                }
            };
        } else if (id.equals(Region.SCROLL_BAR)) {
            return new EliteDangerousSynthStyle() {
                @Override
                public Color getColorForState(SynthContext context, boolean enable, boolean disable, boolean pressed, boolean mouseOver, boolean selected, boolean focused, boolean devault, ColorType type) {
                    if (selected) {
                        if (type.equals(ColorType.FOREGROUND)) return Color.BLACK;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLUE;
                        if (type.equals(ColorType.TEXT_BACKGROUND)) return Color.BLUE;
                    } else if (enable) {
                        if (type.equals(ColorType.FOREGROUND)) return Color.ORANGE;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLACK;
                    }
                    log.warn("UNHANDLED : REG:" + id + " EN:" + (enable ? "T" : "F") + " DIS:" + (disable ? "T" : "F") + " PRS:" + (pressed ? "T" : "F") + " MOV:" + (mouseOver ? "T" : "F") + " SEL:" + (selected ? "T" : "F") + " FOC:" + (focused ? "T" : "F") + " DEV:" + (devault ? "T" : "F") + " - " + type);
                    return Color.WHITE;
                }
            };
        } else {
            return super.getDefaultStyle(component, id);
        }
    }
}
