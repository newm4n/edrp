package com.difotofoto.myeddb.ui.lookfeel.styles;

import java.awt.*;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class ComponentColor {
    Color foreground;
    Color background;
    Color focus;
    Color textForeground;
    Color textBackground;

    public ComponentColor(Color foreground, Color background, Color focus, Color textForeground, Color textBackground) {
        this.foreground = foreground;
        this.background = background;
        this.focus = focus;
        this.textForeground = textForeground;
        this.textBackground = textBackground;
    }

    public Color getForeground() {
        return foreground;
    }

    public void setForeground(Color foreground) {
        this.foreground = foreground;
    }

    public Color getBackground() {
        return background;
    }

    public void setBackground(Color background) {
        this.background = background;
    }

    public Color getFocus() {
        return focus;
    }

    public void setFocus(Color focus) {
        this.focus = focus;
    }

    public Color getTextForeground() {
        return textForeground;
    }

    public void setTextForeground(Color textForeground) {
        this.textForeground = textForeground;
    }

    public Color getTextBackground() {
        return textBackground;
    }

    public void setTextBackground(Color textBackground) {
        this.textBackground = textBackground;
    }
}
