package com.difotofoto.myeddb.ui.lookfeel;

import com.difotofoto.myeddb.ui.lookfeel.styles.EliteDangerousDefaultStyle;

import javax.swing.*;
import javax.swing.plaf.synth.SynthConstants;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthPainter;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.GeneralPath;

/**
 * Created by Ferdinand on 4/26/2016.
 */
public class EliteDangerousPainter extends SynthPainter {

    private void drawDefaultBorder(Graphics g, int x, int y, int w, int h) {
        g.setColor(EliteDangerousDefaultStyle.ED_ORANGE);
        g.drawRect(x, y, w - 1, h - 1);
    }

    private void drawSouth(Graphics g, int x, int y, int w, int h) {
        g.setColor(EliteDangerousDefaultStyle.ED_ORANGE);
        g.drawLine(x, y + h - 1, x + w - 1, y + h - 1);
    }

    private void drawNorth(Graphics g, int x, int y, int w, int h) {
        g.setColor(EliteDangerousDefaultStyle.ED_ORANGE);
        g.drawLine(x, y, x + w - 1, y);
    }

    private void drawEast(Graphics g, int x, int y, int w, int h) {
        g.setColor(EliteDangerousDefaultStyle.ED_ORANGE);
        g.drawLine(x + w - 1, y, x + w - 1, y + h - 1);
    }

    private void drawWest(Graphics g, int x, int y, int w, int h) {
        g.setColor(EliteDangerousDefaultStyle.ED_ORANGE);
        g.drawLine(x, y, x, y + h - 1);
    }

    private void drawVertical(Graphics g, int x, int y, int w, int h) {
        g.setColor(EliteDangerousDefaultStyle.ED_ORANGE);
        g.drawLine(w / 2, y, w / 2, y + h - 1);
    }

    private void drawHorizontal(Graphics g, int x, int y, int w, int h) {
        g.setColor(EliteDangerousDefaultStyle.ED_ORANGE);
        g.drawLine(x, y / 2, x + w - 1, y / 2);
    }

    private void fill(Graphics g, int x, int y, int w, int h, Color color) {
        g.setColor(color);
        g.fillRect(x, y, w, h);
    }

    @Override
    public void paintButtonBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintArrowButtonBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }


    @Override
    public void paintCheckBoxMenuItemBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintCheckBoxBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintColorChooserBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintComboBoxBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintDesktopIconBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintDesktopPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintEditorPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintFileChooserBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintFormattedTextFieldBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintInternalFrameTitlePaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintInternalFrameBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintLabelBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        // drawDefaultBorder(g, x, y, w, h);
        super.paintLabelBorder(context, g, x, y, w, h);
    }

    @Override
    public void paintListBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintListBackground(SynthContext context, Graphics g, int x, int y, int w, int h) {
//        boolean DEFAULT = (context.getComponentState() | SynthConstants.DEFAULT) == SynthConstants.DEFAULT;
//        boolean FOCUSED = (context.getComponentState() | SynthConstants.FOCUSED) == SynthConstants.FOCUSED;
//        boolean SELECTED = (context.getComponentState() | SynthConstants.SELECTED) == SynthConstants.SELECTED;
//
//        boolean DISABLED = (context.getComponentState() | SynthConstants.DISABLED) == SynthConstants.DISABLED;
//        boolean MOUSE_OVER = (context.getComponentState() | SynthConstants.MOUSE_OVER) == SynthConstants.MOUSE_OVER;
//        boolean PRESSED = (context.getComponentState() | SynthConstants.PRESSED) == SynthConstants.PRESSED;
//        boolean ENABLED = (context.getComponentState() | SynthConstants.ENABLED) == SynthConstants.ENABLED;
//
//        if(MOUSE_OVER) {
//            fill(g, x, y, w, h, Color.ORANGE);
//        } else if(SELECTED) {
//            fill(g, x, y, w, h, Color.ORANGE);
//        } else {
//            fill(g, x, y, w, h, Color.BLACK);
//        }
        super.paintListBackground(context, g, x, y, w, h);
    }

    @Override
    public void paintMenuBarBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        // drawDefaultBorder(g, x, y, w, h);
        g.setColor(Color.ORANGE);
        drawSouth(g, x, y, w, h);
    }

    @Override
    public void paintMenuItemBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintMenuBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintOptionPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintPanelBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        // drawDefaultBorder(g, x, y, w, h);
        super.paintPanelBorder(context, g, x, y, w, h);
    }

    @Override
    public void paintPasswordFieldBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintPopupMenuBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintProgressBarBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintProgressBarBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintProgressBarBackground(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        // super.paintProgressBarBackground(context, g, x, y, w, h, orientation);
        fill(g, x, y, w, h, Color.DARK_GRAY);
    }

    @Override
    public void paintProgressBarForeground(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        super.paintProgressBarForeground(context, g, x, y, w, h, orientation);
        fill(g, x, y, w, h, Color.LIGHT_GRAY);
    }

    @Override
    public void paintRadioButtonMenuItemBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintRadioButtonBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintRootPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintScrollBarBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
//        drawDefaultBorder(g, x, y, w, h);
        super.paintScrollBarBorder(context, g, x, y, w, h);
    }

    @Override
    public void paintScrollBarBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
//        drawDefaultBorder(g, x, y, w, h);
        super.paintScrollBarBorder(context, g, x, y, w, h, orientation);
    }

    @Override
    public void paintScrollBarThumbBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintScrollBarTrackBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
//        drawDefaultBorder(g, x, y, w, h);
        super.paintScrollBarTrackBorder(context, g, x, y, w, h);
    }

    @Override
    public void paintScrollBarTrackBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
//        drawDefaultBorder(g, x, y, w, h);
        super.paintScrollBarTrackBorder(context, g, x, y, w, h, orientation);
    }

    @Override
    public void paintScrollPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y + 1, w, h - 1);
//        super.paintScrollPaneBorder(context, g,x,y,w,h);
    }
//
//    @Override
//    public void paintScrollBarThumbBackground(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
//        boolean MOUSE_OVER = (context.getComponentState() | SynthConstants.MOUSE_OVER) == SynthConstants.MOUSE_OVER;
//        if(MOUSE_OVER) {
//            fill(g, x, y, w, h, Color.ORANGE);
//        } else {
//            fill(g, x, y, w, h, Color.DARK_GRAY);
//        }
//    }

    @Override
    public void paintArrowButtonForeground(SynthContext context, Graphics g, int x, int y, int w, int h, int direction) {
        boolean MOUSE_OVER = (context.getComponentState() | SynthConstants.MOUSE_OVER) == SynthConstants.MOUSE_OVER;
        if (MOUSE_OVER) {
            fill(g, x, y, w, h, Color.ORANGE);
        } else {
            fill(g, x, y, w, h, Color.DARK_GRAY);
        }

        g.setColor(Color.BLACK);
        GeneralPath p = new GeneralPath();
        p.moveTo(0, 3);
        p.lineTo(2, -2);
        p.lineTo(-2, -2);
        p.closePath();

        Graphics2D g2d = (Graphics2D) g;
        g2d.setPaint(Color.BLACK);

        switch (direction) {
            case SwingConstants.EAST:
                p.transform(AffineTransform.getRotateInstance(90));
                g2d.fill(p);
                break;
            case SwingConstants.WEST:
                p.transform(AffineTransform.getRotateInstance(270));
                g2d.fill(p);
                break;
            case SwingConstants.NORTH:
                g2d.fill(p);
                break;
            case SwingConstants.SOUTH:
                p.transform(AffineTransform.getRotateInstance(180));
                g2d.fill(p);
                break;
        }
    }
//
//    @Override
//    public void paintMenuBarBackground(SynthContext context, Graphics g, int x, int y, int w, int h) {
//        fill(g, x, y, w, h, Color.BLACK);
//    }


    @Override
    public void paintSeparatorBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawHorizontal(g, x, y, w, h);
    }

    @Override
    public void paintSeparatorBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawHorizontal(g, x, y, w, h);
    }

    @Override
    public void paintSliderBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintSliderBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintSliderThumbBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintSliderTrackBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintSliderTrackBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintSpinnerBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintSplitPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTabbedPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTabbedPaneTabAreaBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTabbedPaneTabAreaBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTabbedPaneTabBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int tabIndex) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTabbedPaneTabBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int tabIndex, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTableHeaderBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTableHeaderBackground(SynthContext context, Graphics g, int x, int y, int w, int h) {
        // fill(g, x, y, w, h, Color.BLACK);
        super.paintTableHeaderBackground(context, g, x, y, w, h);
    }

    @Override
    public void paintTableBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTableBackground(SynthContext context, Graphics g, int x, int y, int w, int h) {
        boolean SELECTED = (context.getComponentState() | SynthConstants.SELECTED) == SynthConstants.SELECTED;
        if (SELECTED) {
            fill(g, x, y, w, h, Color.BLUE);
        } else {
            fill(g, x, y, w, h, Color.BLACK);
        }
        //super.paintTableBackground(context, g, x, y, w, h);
    }

    @Override
    public void paintTextAreaBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTextPaneBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTextFieldBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToggleButtonBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToolBarBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToolBarBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToolBarContentBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToolBarContentBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToolBarDragWindowBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToolBarDragWindowBorder(SynthContext context, Graphics g, int x, int y, int w, int h, int orientation) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintToolTipBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTreeBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintTreeCellBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }

    @Override
    public void paintViewportBorder(SynthContext context, Graphics g, int x, int y, int w, int h) {
        drawDefaultBorder(g, x, y, w, h);
    }
}
