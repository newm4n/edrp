package com.difotofoto.myeddb.ui.lookfeel.styles;

import org.apache.log4j.Logger;

import javax.swing.*;
import javax.swing.plaf.synth.ColorType;
import javax.swing.plaf.synth.Region;
import javax.swing.plaf.synth.SynthContext;
import javax.swing.plaf.synth.SynthStyle;
import java.awt.*;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class EliteDangerousToolTipStyle extends EliteDangerousDefaultStyle {
    Logger log = Logger.getLogger(EliteDangerousToolTipStyle.class);

    @Override
    public SynthStyle styleForRegion(JComponent component, Region id) {
        if (id.equals(Region.TOOL_TIP)) {
            return new EliteDangerousSynthStyle() {
                @Override
                public Color getColorForState(SynthContext context, boolean enable, boolean disable, boolean pressed, boolean mouseOver, boolean selected, boolean focused, boolean devault, ColorType type) {
                    if (enable) {
                        if (type.equals(ColorType.FOREGROUND)) return ED_ORANGE;
                        if (type.equals(ColorType.BACKGROUND)) return Color.BLACK;
                        if (type.equals(ColorType.TEXT_FOREGROUND)) return ED_ORANGE;
                        if (type.equals(ColorType.TEXT_BACKGROUND)) return Color.BLACK;
                    }
                    log.warn("UNHANDLED : REG:" + id + " EN:" + (enable ? "T" : "F") + " DIS:" + (disable ? "T" : "F") + " PRS:" + (pressed ? "T" : "F") + " MOV:" + (mouseOver ? "T" : "F") + " SEL:" + (selected ? "T" : "F") + " FOC:" + (focused ? "T" : "F") + " DEV:" + (devault ? "T" : "F") + " - " + type);
                    return Color.WHITE;
                }

                @Override
                public Insets getInsets(SynthContext context, Insets insets) {
                    return new Insets(10, 10, 10, 10);
                }
            };
        } else {
            return super.getDefaultStyle(component, id);
        }
    }
}
