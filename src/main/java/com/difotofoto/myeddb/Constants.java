package com.difotofoto.myeddb;

import java.awt.*;

/**
 * Created by Ferdinand on 4/27/2016.
 */
public class Constants {

    public static Color ED_ORANGE = new Color(175, 82, 10);
    public static Color ED_RED = new Color(191, 10, 10);
    public static Color ED_YELLOW = new Color(181, 127, 16);
    public static Color ED_DARKRED = new Color(66, 20, 10);

    public static Color DARK_GREEN = new Color(0, 75, 32);
    public static Color LIGHT_GREEN = new Color(139, 253, 188);

}
