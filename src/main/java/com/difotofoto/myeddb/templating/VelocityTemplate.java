package com.difotofoto.myeddb.templating;

import com.difotofoto.myeddb.ui.editor.TradeRoute;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.List;

/**
 * Created by Ferdinand on 4/30/2016.
 */
public class VelocityTemplate {

    public static VelocityTemplate htmlTemplate = null;
    public static VelocityTemplate jsonTemplate = null;
    public static VelocityTemplate txtTemplate = null;

    static {
        try {
            htmlTemplate = new VelocityTemplate(VelocityTemplate.class.getResourceAsStream("/HTML_TEMPLATE.VM"));
            jsonTemplate = new VelocityTemplate(VelocityTemplate.class.getResourceAsStream("/JSON_TEMPLATE.VM"));
            txtTemplate = new VelocityTemplate(VelocityTemplate.class.getResourceAsStream("/TXT_TEMPLATE.VM"));
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    private String templateString;

    public VelocityTemplate(String templateString) {
        setTemplateString(templateString);
    }

    public VelocityTemplate(InputStream is) throws IOException {
        byte[] buffer = new byte[2000];
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        while(true) {
            int read = is.read(buffer);
            if(read >= 0) {
                if(read > 0) {
                    baos.write(buffer, 0, read);
                }
            } else {
                break;
            }
        }
        setTemplateString(new String(baos.toByteArray()));
    }

    public String processTemplate(List<TradeRoute> routes) {
        VelocityContext context = new VelocityContext();
        context.put("routes", routes);
        StringWriter sw = new StringWriter();
        Velocity.evaluate(context, sw, "Trade Route", templateString);
        return sw.toString();
    }

    public String getTemplateString() {
        return templateString;
    }

    public void setTemplateString(String templateString) {
        this.templateString = templateString;
    }
}
