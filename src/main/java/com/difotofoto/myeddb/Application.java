package com.difotofoto.myeddb;

import com.difotofoto.myeddb.ui.MainFrame;
import org.apache.log4j.Logger;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class Application {

    public static Logger log = Logger.getLogger(Application.class);
    public static boolean debug = false;

    public static boolean updateIndex = true;

    public static void main(String[] args) {
        MainFrame mainFrame = new MainFrame();
        mainFrame.setVisible(true);
    }


}
