package com.difotofoto.myeddb.importer;

import com.difotofoto.myeddb.Application;
import com.difotofoto.myeddb.model.Commodity;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class CommodityImporter {

    public static Logger log = Logger.getLogger(CommodityImporter.class);

    public Map<Integer, Commodity> importJson() throws IOException {
        log.debug("Importing ... ");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        InputStream is = null;
        if (Application.debug == true) {
            is = CommodityImporter.class.getResourceAsStream("/commodities.json");
        } else {
            is = new ByteArrayInputStream(HttpImporter.fetchV1("https://eddb.io/archive/v4/commodities.json", new ProgressBarProgressListener("https://eddb.io/archive/v4/commodities.json")));
        }

        List<Commodity> commodities = objectMapper.readValue(is,
                TypeFactory.defaultInstance().constructCollectionType(List.class,
                        Commodity.class));
        Map<Integer, Commodity> map = new HashMap<Integer, Commodity>();
        for (Commodity c : commodities) {
            map.put(c.getId(), c);
        }
        return map;
    }


}
