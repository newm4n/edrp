package com.difotofoto.myeddb.importer;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Ferdinand on 3/31/2016.
 */
public class ProgressBarProgressListener extends JFrame implements DownloadProgressListener {

    JProgressBar progress = new JProgressBar();


    private int percentage;
    private long length = -1;
    private long downloaded;

    public ProgressBarProgressListener(String url) {
        this.setTitle(url);
        this.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.setLayout(new BorderLayout());
        progress.setPreferredSize(new Dimension(400, 20));
        progress.setMinimum(0);
        progress.setMaximum(100);
        panel.add(progress, BorderLayout.CENTER);
        this.setContentPane(panel);
        this.pack();

        this.setLocation(
                (Toolkit.getDefaultToolkit().getScreenSize().width / 2) - (this.getSize().width / 2),
                (Toolkit.getDefaultToolkit().getScreenSize().height / 2) - (this.getSize().height / 2));
    }

    @Override
    public void contentLength(long length) {
        this.length = length;
    }

    @Override
    public void start() {
        if (length > 0) {
            progress.setIndeterminate(false);
        } else {
            progress.setIndeterminate(true);
        }
        Thread t = new Thread() {
            public void run() {
                ProgressBarProgressListener.this.setVisible(true);
            }
        };
        t.start();
    }

    @Override
    public void finish() {
        this.setVisible(false);
    }

    @Override
    public void downloaded(long downloaded) {
        this.downloaded = downloaded;
        double dl = (((double) downloaded) / ((double) length)) * 100d;
        percentage = (int) Math.round(dl);
        progress.setValue(percentage);
        progress.setString(percentage + " %");
    }
}
