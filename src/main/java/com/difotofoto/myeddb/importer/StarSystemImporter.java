package com.difotofoto.myeddb.importer;

import com.difotofoto.myeddb.Application;
import com.difotofoto.myeddb.model.StarSystem;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class StarSystemImporter {

    public static Logger log = Logger.getLogger(StarSystemImporter.class);

    public static void main(String[] args) throws Exception {
        StarSystemImporter ssi = new StarSystemImporter();
        Map<Integer, StarSystem> smap = ssi.importJson();
        List<String> strs = new ArrayList<>();
        for (StarSystem ss : smap.values()) {
            StringTokenizer st = new StringTokenizer(ss.getName());
            while (st.hasMoreTokens()) {
                String tok = st.nextToken();
                if (!strs.contains(tok)) {
                    strs.add(tok);
                }
            }
        }
        Collections.sort(strs);
        File f = new File("C:\\Users\\Ferdinand\\Downloads\\syswords.txt");
        if (f.exists()) f.delete();
        f.createNewFile();
        PrintWriter pw = new PrintWriter(new FileWriter(f));
        for (String s : strs) {
            pw.println(s);
        }
        pw.close();
        System.exit(0);
    }

    public Map<Integer, StarSystem> importJson() throws IOException {
        log.debug("Importing ... ");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        InputStream is = null;
        if (Application.debug == true) {
            is = CommodityImporter.class.getResourceAsStream("/systems.json");
        } else {
            is = new ByteArrayInputStream(HttpImporter.fetchV1("https://eddb.io/archive/v4/systems.json", new ProgressBarProgressListener("https://eddb.io/archive/v4/systems.json")));
        }

        List<StarSystem> systems = objectMapper.readValue(is,
                TypeFactory.defaultInstance().constructCollectionType(List.class,
                        StarSystem.class));
        Map<Integer, StarSystem> map = new HashMap<Integer, StarSystem>();
        for (StarSystem c : systems) {
            map.put(c.getId(), c);
        }
        return map;
    }

}
