package com.difotofoto.myeddb.importer;

import com.difotofoto.myeddb.Application;
import com.difotofoto.myeddb.model.Station;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.*;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class StationImporter {
    public static Logger log = Logger.getLogger(StationImporter.class);

    public static void main(String[] args) throws Exception {
        StationImporter ssi = new StationImporter();
        Map<Integer, Station> smap = ssi.importJson();
        List<String> strs = new ArrayList<>();
        for (Station ss : smap.values()) {
            StringTokenizer st = new StringTokenizer(ss.getName());
            while (st.hasMoreTokens()) {
                String tok = st.nextToken();
                if (!strs.contains(tok)) {
                    strs.add(tok);
                }
            }
        }
        Collections.sort(strs);
        File f = new File("C:\\Users\\Ferdinand\\Downloads\\statwords.txt");
        if (f.exists()) f.delete();
        f.createNewFile();
        PrintWriter pw = new PrintWriter(new FileWriter(f));
        for (String s : strs) {
            pw.println(s);
        }
        pw.close();
        System.exit(0);
    }

    public Map<Integer, Station> importJson() throws IOException {
        log.debug("Importing ... ");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);


        InputStream is = null;
        if (Application.debug == true) {
            is = CommodityImporter.class.getResourceAsStream("/stations.json");
        } else {
            is = new ByteArrayInputStream(HttpImporter.fetchV1("https://eddb.io/archive/v4/stations.json", new ProgressBarProgressListener("https://eddb.io/archive/v4/stations.json")));
        }

        List<Station> stations = objectMapper.readValue(is,
                TypeFactory.defaultInstance().constructCollectionType(List.class,
                        Station.class));
        Map<Integer, Station> map = new HashMap<Integer, Station>();
        for (Station c : stations) {
            //if(c.getStarSystem().getAllegianceEnum().equals(AllegianceEnum.ALLIANCE)) {
            map.put(c.getId(), c);
            //}
        }
        return map;
    }
}
