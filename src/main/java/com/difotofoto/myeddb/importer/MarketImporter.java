package com.difotofoto.myeddb.importer;

import com.difotofoto.myeddb.Application;
import com.difotofoto.myeddb.model.Commodity;
import com.difotofoto.myeddb.model.Market;
import com.difotofoto.myeddb.model.Station;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ferdinand on 3/10/2016.
 */
public class MarketImporter {

    public static Logger log = Logger.getLogger(MarketImporter.class);

    public Map<Integer, Market> importMarket(Map<Integer, Commodity> commodities, Map<Integer, Station> stations) throws IOException {
        log.debug("Importing ... ");

        InputStream is = null;
        if (Application.debug == true) {
            is = MarketImporter.class.getResourceAsStream("/listings.csv");
        } else {
            is = new ByteArrayInputStream(HttpImporter.fetchV1("https://eddb.io/archive/v4/listings.csv", new ProgressBarProgressListener("https://eddb.io/archive/v4/listings.csv")));
        }


        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String line = br.readLine();
        Map<Integer, Market> markets = new HashMap<Integer, Market>();
        while (true) {
            line = br.readLine();
            if (line == null) break;
            String[] split = line.split(",");
            Market m = new Market();
            m.setSupply(Integer.parseInt(split[3]));
            m.setBuy(Long.parseLong(split[4]));
            m.setSell(Long.parseLong(split[5]));
            m.setDemand(Integer.parseInt(split[6]));
            Station station = stations.get(Integer.parseInt(split[1]));
            if (station != null) {
                m.setStation(station);
                station.getMarkets().add(m);
            } else {
                System.out.println("Market ID : " + m.getId() + " have invalid station id " + split[1]);
            }
            m.setId(Integer.parseInt(split[0]));
            Commodity commodity = commodities.get(Integer.parseInt(split[2]));
            if (commodity != null) {
                m.setCommodity(commodity);
            } else {
                System.out.println("Market ID : " + m.getId() + " have invalid commodity id " + split[2]);
            }
            markets.put(m.getId(), m);
        }
        return markets;
    }

}
