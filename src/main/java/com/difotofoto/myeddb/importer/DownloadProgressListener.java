package com.difotofoto.myeddb.importer;

/**
 * Created by Ferdinand on 3/17/2016.
 */
public interface DownloadProgressListener {
    public void contentLength(long length);

    public void start();

    public void finish();

    public void downloaded(long downloaded);
}
