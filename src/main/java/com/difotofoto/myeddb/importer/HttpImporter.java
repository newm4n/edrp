package com.difotofoto.myeddb.importer;

import org.apache.http.Header;
import org.apache.http.HttpVersion;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.log4j.Logger;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

/**
 * Created by ferdinand on 3/10/2016.
 */
public class HttpImporter {

    public static Logger log = Logger.getLogger(HttpImporter.class);

    public static byte[] fetchV2(String url, DownloadProgressListener dpl) throws IOException {
        log.debug("Start fetching " + url);
        CloseableHttpClient httpclient = null;
        HttpGet httpget = null;
        CloseableHttpResponse response = null;
        try {
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustStrategy() {
                @Override
                public boolean isTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                    return true;
                }
            });
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    builder.build());
            httpclient = HttpClients.custom().setSSLSocketFactory(
                    sslsf).build();

            httpget = new HttpGet(url);
            response = httpclient.execute(httpget);

            Header contentLenHeader = response.getFirstHeader("Content-Length");

            if (contentLenHeader != null) {
                log.debug("Content Length : " + contentLenHeader.getValue());
                if (dpl != null) dpl.contentLength(Long.parseLong(contentLenHeader.getValue()));
            }

            if (dpl != null) dpl.start();

            InputStream is = response.getEntity().getContent();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[2000];
            long readed = 0;
            while (true) {
                int read = is.read(buffer);
                if (read >= 0) {
                    if (read > 0) {
                        readed += read;
                        baos.write(buffer, 0, read);
                        if (dpl != null) dpl.downloaded(readed);
                    }
                } else {
                    break;
                }
            }
            is.close();
            log.info(baos.size() + " bytes read from " + url);
            return baos.toByteArray();
        } catch (Exception e) {
            throw new IOException(e);
        } finally {
            if (response != null)
                response.close();
            if (httpclient != null)
                httpclient.close();
            if (dpl != null) dpl.finish();

        }

    }

    public static byte[] fetchV1(String url, DownloadProgressListener dpl) throws IOException {
        log.debug("Start fetching " + url);

        KeyStore keyStore = null;

        FileInputStream instream = null;
        HttpGet httpget = null;
        CloseableHttpClient httpclient = null;
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            MySSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            httpclient = new DefaultHttpClient(ccm, params);

            httpget = new HttpGet(url);
            CloseableHttpResponse response = httpclient.execute(httpget);


            Header contentLenHeader = response.getFirstHeader("Content-Length");

            if (contentLenHeader != null) {
                log.debug("Content Length : " + contentLenHeader.getValue());
                if (dpl != null) dpl.contentLength(Long.parseLong(contentLenHeader.getValue()));
            }

            if (dpl != null) dpl.start();

            InputStream is = response.getEntity().getContent();
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            byte[] buffer = new byte[2000];
            long readed = 0;
            while (true) {
                int read = is.read(buffer);
                if (read >= 0) {
                    if (read > 0) {
                        readed += read;
                        baos.write(buffer, 0, read);
                        if (dpl != null) dpl.downloaded(readed);
                    }
                } else {
                    break;
                }
            }
            is.close();
            log.info(baos.size() + " bytes read from " + url);
            return baos.toByteArray();

        } catch (KeyStoreException e) {
            throw new IOException(e);
        } catch (CertificateException e) {
            throw new IOException(e);
        } catch (NoSuchAlgorithmException e) {
            throw new IOException(e);
        } catch (UnrecoverableKeyException e) {
            throw new IOException(e);
        } catch (KeyManagementException e) {
            throw new IOException(e);
        } finally {
            if (instream != null)
                instream.close();
            if (httpclient != null)
                httpclient.close();
            if (dpl != null) dpl.finish();
        }
    }

}
