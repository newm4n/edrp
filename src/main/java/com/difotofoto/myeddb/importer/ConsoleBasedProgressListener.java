package com.difotofoto.myeddb.importer;

import org.apache.log4j.Logger;

/**
 * Created by Ferdinand on 3/17/2016.
 */
public class ConsoleBasedProgressListener implements DownloadProgressListener {

    private static Logger log = Logger.getLogger(ConsoleBasedProgressListener.class);

    private long length = -1;
    private int percent = 0;
    private long startTime;

    @Override
    public void contentLength(long length) {
        this.length = length;
        log.info("Downloading " + length + " bytes.");
    }

    @Override
    public void start() {
        log.info("Download start.");
        startTime = System.currentTimeMillis();
    }

    @Override
    public void finish() {
        log.info("Download finish. in " + (System.currentTimeMillis() - startTime) + " ms.");
    }

    @Override
    public void downloaded(long downloaded) {
        if (length > 0) {
            double dl = (((double) downloaded) / ((double) length)) * 100d;
            int p = (int) Math.round(dl);

            double dsec = (((double) System.currentTimeMillis()) - ((double) startTime)) / 1000d;
            int sec = (int) Math.floor(dsec);

            int speed = 0;
            if (sec > 0) {
                speed = (int) (downloaded / sec);
            }

            if (p != percent) {
                log.info(p + "% at " + speed + " Bps");
                percent = p;
            }
        }
    }
}
