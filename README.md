# EDRP #

**EDRP** is stand for **E**lite **D**angerous **R**oute **P**lanner. Its a simple utility that have one single goal, to help you make a profitable trade route in Elite Dangerous universe. I know, EDRP is a horrible name and to makeup for that, I call it simply E-Drop.

### What EDRP can do for you ? ###

* Help you organize your trade route.
* Figure out which commodity to trade with the highest profit in the following cases :
	* Most profitable commodity to trade between two system. (EDRP will search commodities in all stations in both system for the highest profit).
	* Most profitable commodity to trade between to station. (EDRP will search commodities in both stations to trade for highest profit).
	* You've bought a commodity in one station, and you want to find any station in a specific system where you can sell your commodity with highest price.
* Create a trade route chain fully customizable between the hops where you can specify system or station or commodity in between and EDRP will find the best price between them.
* Export your trade route diagram into PNG file.
* Export your trade route diagram into HTML, TXT or JSON.

### How do I get set up? ###

* EDRP is a JAVA-Swing based application. Thus, it need a JAVA VM installed, Require version 8 or later.
* You need :
	* Java 8.0 ( download from [ Here ](http://www.oracle.com/technetwork/java/javase/downloads/index.html) )
	* EDRP Distribution ( download from [ Here ](https://bitbucket.org/newm4n/edrp/downloads) )
* Install the Java 8.
* Download EDRP into your favourite folder.

### Running EDRP ###

If you install using the installer provided by Oracle and properly finish the process, you can simply double click the EDRP's JAR file and it will run.
But in any case that it wont :

* Start your command line console.
	* Hit the "Start" button.
	* Type "cmd" in the run field and enter.
	* A command line console will be opened.
* Verify that java can be executed.
	* Type "java -version"

```
C:\Users\Ferdinand>java -version
java version "1.8.0_05"
Java(TM) SE Runtime Environment (build 1.8.0_05-b13)
Java HotSpot(TM) 64-Bit Server VM (build 25.5-b02, mixed mode)
```

* Move to the directory where you've downloaded the EDRP jar file. And execute java on the jar.

```
cd location\for\download
java -jar edrp.jar
```
	
### Assitance ? ###

You may email me when you find something ( ferdinand.neman at gmail.com )

### Disclaimer ###

* Elite Dangerous is a trademark owned by [Frontier Development] (http://www.frontier.co.uk/).
* EDRP has no aviation with Frontier Development.
* EDRP uses data and information provided by [ EDDB.io ](https://eddb.io/api)